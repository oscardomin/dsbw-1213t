import com.typesafe.startscript.StartScriptPlugin

seq(StartScriptPlugin.startScriptForClassesSettings: _*)

seq(yuiSettings: _*)

// JS Compression

YuiCompressorKeys.options in (Compile, YuiCompressorKeys.jsCompressor) += yuiCompressor.Opts.js.nomunge

unmanagedResourceDirectories in (Compile, YuiCompressorKeys.jsResources) <+= sourceDirectory / "main/docroot/scripts"

resourceManaged in (Compile, YuiCompressorKeys.jsCompressor) <<= sourceDirectory / "main/docroot/min"

// CSS Compression (yui-compressor doesn't handle @import's within .css files)

unmanagedResourceDirectories in (Compile, YuiCompressorKeys.cssResources) <+= sourceDirectory / "main/docroot/css"

resourceManaged in (Compile, YuiCompressorKeys.cssCompressor) <<= sourceDirectory / "main/docroot/min"

resolvers += "Sonatype OSS Snapshots" at "https://oss.sonatype.org/content/repositories/snapshots"

resolvers += "scalatools" at "https://oss.sonatype.org/content/groups/scala-tools/"

resolvers += Resolver.url("sbt-plugin-releases", url("http://scalasbt.artifactoryonline.com/scalasbt/sbt-plugin-releases/"))(Resolver.ivyStylePatterns)

organization := "edu.upc.fib"

name := "dsbw-1213T"

version := "0.1.0-SNAPSHOT"

scalaVersion := "2.10.0"

libraryDependencies ++= Seq(
  "org.scalatest" % "scalatest_2.10" % "1.9.1" % "test",
  "org.mockito" % "mockito-all" % "1.9.0" % "test",
  "org.eclipse.jetty" % "jetty-servlet" % "8.1.0.RC5",
  "javax.servlet" % "servlet-api" % "2.5",
  "org.mongodb" %% "casbah" % "2.5.0",
	"com.novus" %% "salat" % "1.9.2",
	"org.ocpsoft.prettytime" % "prettytime" % "3.1.0.Final"
)

net.virtualvoid.sbt.graph.Plugin.graphSettings


