/*
 * jQuery CSSEmoticons plugin 0.2.9
 *
 * Copyright (c) 2010 Steve Schwartz (JangoSteve)
 *
 * Dual licensed under the MIT and GPL licenses:
 *   http://www.opensource.org/licenses/mit-license.php
 *   http://www.gnu.org/licenses/gpl.html
 *
 * Date: Sun Oct 22 1:00:00 2010 -0500
 */
(function($) {
    $.fn.anime = function(options) {

      var opts = $.extend({}, $.fn.anime.defaults, options);

      var escapeCharacters = [ ")", "(", "*",  "|"];

      var twoCharacterEmoticons = [":)", ":0", ";)", ":D", ":(", ";D", ":P", ":O", ":s", ":|", ":*" ];

      var specialRegex = new RegExp( '(\\' + escapeCharacters.join('|\\') + ')', 'g' );

      var preMatch = '(^|)';

      for ( var i=twoCharacterEmoticons.length-1; i>=0; --i ){
        twoCharacterEmoticons[i] = twoCharacterEmoticons[i].replace(specialRegex,'\\$1');
        twoCharacterEmoticons[i] = new RegExp( preMatch+'(' + twoCharacterEmoticons[i] + ')', 'g' );
      }

      return this.each(function() {
        var container = $(this);

        $(twoCharacterEmoticons).each(function(){
          container.html(container.html().replace(this,
  			"$1<span class='css-emoticon un-transformed-emoticon animated-emoticon spaced-emoticon'>$2</span>"));
        });
        setTimeout(function(){$('.un-transformed-emoticon').removeClass('un-transformed-emoticon');}, 500);
      });
    }

  $.fn.hashtagging = function(hash,chirpElement) {
          //To linkify #hashtags
          var toReplace = /#([a-zA-Z0-9_]+)/g;
          var textChirp = $('p.animate', chirpElement).html().replace(toReplace, '<a href=# id=hash>#$1</a>');
          $('p.animate', chirpElement).html(textChirp);
          //To linkify @usernames
          var toReplace = /@([a-zA-Z0-9_]+)/g;
          var textChirp = $('p.animate', chirpElement).html().replace(toReplace, '<a href=# class="userMention" usernameauthor="$1">@$1</a>');
          $('p.animate', chirpElement).html(textChirp);
  }

  $.fn.youtube = function(chirpElement) {
        var container = $(this);
        // regexp for several youtube link formats
        var ytRegexp = /(?:http|https|)(?::\/\/|)(?:www.|)(?:youtu\.be\/|youtube\.com(?:\/embed\/|\/v\/|\/watch\?v=|\/ytscreeningroom\?v=|\/feeds\/api\/videos\/|\/user\S*[^\w\-\s]|\S*[^\w\-\s]))([\w\-]{11})[a-z0-9;:@?&%=+\/\$_.-]*/
        var find = container.text().search(ytRegexp);
        //regexp for images
        var imgRegexp = /(https?:\/\/[\S]+\.(png|PNG|jpg|JPG|jpeg|JPEG|GIF|gif))/
        var imgFind = container.text().search(imgRegexp);
        if (find >= 0 || imgFind >= 0){
            if ((find <= imgFind && find != -1) || imgFind == -1){
                 var dir = container.text().substring(31+find,42+find);
                 var videoURL = "http://youtube.com/watch?v=" + dir;
                 var thumbnailURL = "http://img.youtube.com/vi/" + dir + "/1.jpg";
                 var iframeURL = "http://www.youtube.com/embed/" + dir + "?wmode=transparent&autoplay=1";
                 var apiURL = "https://gdata.youtube.com/feeds/api/videos/" + dir + "?v=2&alt=json-in-script&callback=?";
                 $('.displayed',chirpElement).attr('iframeURL',iframeURL);
                 createThumbnail(false, chirpElement, videoURL, thumbnailURL, apiURL);
            }
            else{
                var imgUrl = container.text().match(imgRegexp);
                var urlImage = imgUrl[0];
                $('.displayed',chirpElement).attr('urlImage', urlImage);
                $(".play", chirpElement).hide();
                createThumbnail(true, chirpElement, urlImage, urlImage, "");
                if(urlImage.length >= 35){
                    urlImage = urlImage.substring(0,35).concat("...");
                }
                $(".info-small", chirpElement).html(urlImage);
            }
        }
        else $(".displayed", chirpElement).hide();
        container.html(linkify(container.text()));
  }

  function createThumbnail(isImage, chirpElement, URL, thumbnailURL, apiURL) {
       // Get video information and set the title.
       var title;
       if(isImage) $(".info", chirpElement).html("<b><a href='" + URL + "' target='_blank'>Full size image</a></b>");
       else {
             $.getJSON(apiURL).done(function(data) {
                $(".info", chirpElement).html("<b><a href='" + URL + "' target='_blank'>" + data.entry.title.$t + "</a></b>");
             });
       }
       // Set the thumbnail image for the video.
       $(".thumb", chirpElement).attr("src", thumbnailURL);
  }

  // Switch to the iframe when the image is clicked.
  $(".displayed").live('click',function(e) {
        var iframeURL = $(this).attr('iframeURL');
        var urlImage = $(this).attr('urlImage');
        if (iframeURL != undefined){
            $(".preview", this).html("<iframe width='400' height='250' src='" + iframeURL + "' frameborder='0' allowfullscreen></iframe>");
        }
        else if (urlImage != undefined){
            $(".preview", this).html("<img class='imgDisplayed' src='" + urlImage + "' </img>");
        }
        $(".preview", this).css("float", "none");
        $(this).css("background-color","white");
        $(this).css("border","0px");
        $('.info-small', this).hide();
        $('.info', this).hide();
  });

  function linkify(text){
      if (text) {
          text = text.replace(
              /((https?\:\/\/)|(www\.))(\S+)(\w{2,4})(:[0-9]+)?(\/|\/([\w#!:.?+=&%@!\-\/]))?/gi,
              function(url){
                  var full_url = url;
                  if (!full_url.match('^https?:\/\/')) {
                      full_url = 'http://' + full_url;
                  }
                  return '<a href="' + full_url + '"  target="_blank">' + url + '</a>';
              }
          );
      }
      return text;
  }

})(jQuery);

