/* Commenting Rules
* 1. @author name
* 2. Description of function
* 3. @param -> parameters name + data type + (optional) descr
* 4. (optional) @return -> return datatype / format
*/

(function(){

/***********************************************************************************************************************
*                                                                                                                      *
*           Global vars                                                                                                *
*                                                                                                                      *
***********************************************************************************************************************/

    var api = {
                follow : "/api/follow",
                unfollow : "/api/unfollow",
                getUser : "/api/getUser",
                newChirp : "/api/newChirp",
                login : "/api/login",
                logout: "/api/logout",
                rechirp : "/api/rechirp",
                hashtag : "/api/hashtag",
                chirps : "/api/chirps",
                ownTimeline : "/api/ownTimeline",
                userTimeline : "/api/userTimeline",
                myTimeline : "/api/myTimeline",
                followees : "/api/getFollowees",
                favorites : "/api/getFavorites",
                favorite : "/api/favorite",
                unfavorite : "/api/unfavorite",
                privateMessages : "/api/privateMessages",
                reply: "/api/replyChirp",
                numPM: "/api/numPM",
                send: "/api/send"
    };

    var timelineType = {
                generalT: "generalTimelinePage",
                myT: "myTimelinePage",
                ownT: "ownTimelinePage",
                userT: "userTimelinePage",
                hashtagT: "hashtagTimelinePage",
                myFav: "ownFavoritePage",
                myPM: "myPrivateMessagesPage"
    }

    var chirps = []
    var PM = []
    var favoriteChirps = []
    var chirpers = {}


/***********************************************************************************************************************
*                                                                                                                      *
*           Private functions                                                                                           *
*                                                                                                                      *
***********************************************************************************************************************/

    function renderChirps(){
        var id;
        $(chirps).each(function(){
            if(id !== this.id)
            {
                addChirp(this, false);
                id = this.id;
            }
        });
    }

    function renderReplies(chirp){
        $(chirp.replies).each(function(){
            addReply(this, false, chirp.id);
        });
    }

    function renderPMs(){
            $(PM).each(function(){
                addPM(this, false);
            });
        }

    /* @author : Slodoban
     * Paint the chirps in the Timetable stored in var chirps[]
     * @param : chirps[]: Stores the chirps that will be displayed
     * @return : Paint the chirps
     */
    function addChirp(chirp, newChirp){
        if(newChirp == false)
            var chirpElement = $("#chirpTemplate.template").clone().appendTo("#chirps").attr("class", "not_chirp_template");
        else
            var chirpElement = $("#chirpTemplate.template").clone().prependTo("#chirps").attr("class", "not_chirp_template");

        var rechirpElement = $("#rechirp",chirpElement);
        var replyElement = $("#reply",chirpElement);
        var favoriteElement = $("#favorite",chirpElement);

        rechirpElement.attr("idChirp",chirp.id);
        rechirpElement.attr("idChirper",chirp.author.username);

        replyElement.attr("idChirp",chirp.id);
        replyElement.attr("idChirper",chirp.author.username);

        favoriteElement.attr("idChirp",chirp.id);
        favoriteElement.attr("idChirper",chirp.author.username);

        if(chirp.author.username != chirp.chirper.username) {
            var spanRechirper = $("span.rechirper",chirpElement);
            spanRechirper.text("rechirped by @"+ chirp.chirper.username);
            spanRechirper.attr("usernameRechirper",chirp.chirper.username);
            chirpElement.css("background-color","#bfeeff");
            $('img.is_rechirp',chirpElement).show();
            spanRechirper.show();
        }

        //Load small avatar of the author of the chirp and its attrs are saved
        var smallAvatar = $(".small_avatar",chirpElement);
        smallAvatar.attr("src",chirp.author.avatar);
        smallAvatar.attr("usernameAuthor",chirp.author.username);

        //Display the name of the author and its attrs are saved
        var spanName =  $("span.name",chirpElement);
        spanName.text(chirp.author.name);
        spanName.attr("usernameAuthor",chirp.author.username);

        //Display the username of the author and its attrs are saved
        var spanUsername =  $("span.username",chirpElement);
        spanUsername.text("@" + chirp.author.username);
        spanUsername.attr("usernameAuthor",chirp.author.username);

        $("p",chirpElement).text(chirp.message);
        $("date",chirpElement).text(chirp.date);

        var animate = $('.animate',chirpElement);
        //Detect youtube links in a chirp
        animate.youtube(chirpElement);

        //Detect emoticons in a chirp
        animate.anime();

        //Detect hashtags in a chirp
        animate.hashtagging(chirp.hashtags,chirpElement);
        //console.log(chirp.favorite);
        if (chirp.favorite) {
            event.preventDefault();

             favicon = $('#favTinyIcon',chirpElement);
             //console.log(favicon);
             is_favorited = chirpElement.children(".is_favorited");
             rechirp_as_favorited = chirpElement.children(".rechirp_is_favorited");
             is_rechirp = chirpElement.children(".is_rechirp");

             if(favicon.attr("src") == "img/favorite-icon.png") {
                 favicon.attr("src","img/favorite-icon2.png");
                 //$(this).css("color","#d47a02");
                 if(is_rechirp.is(":visible")) {
                    rechirp_as_favorited.css("display","block");
                    is_rechirp.css("display","none");
                 }
                 else is_favorited.css("display","block");
             }
             else {
                 favicon.attr("src","img/favorite-icon.png");
                 //$(this).css("color","")
                 if(rechirp_as_favorited.is(":visible")) {
                    rechirp_as_favorited.css("display","none");
                    is_rechirp.css("display","block");
                 }
                 else is_favorited.css("display","none");
             }
        }

        //Render replies
        if(chirp.replies.length > 0) {
            renderReplies(chirp);
        }
    }

    function addPM(pm, newPM){
        if(newPM == false)
            var chirpElement = $("#chirpTemplate.template").clone().appendTo("#chirps").attr("class", "not_chirp_template");
        else
            var chirpElement = $("#chirpTemplate.template").clone().prependTo("#chirps").attr("class", "not_chirp_template");

        //Load small avatar of the author of the chirp and its attrs are saved
        var smallAvatar = $(".small_avatar",chirpElement);
        smallAvatar.attr("src",pm.from.avatar);
        smallAvatar.attr("usernameAuthor",pm.from.username);

        //Display the name of the author and its attrs are saved
        var spanName =  $("span.name",chirpElement);
        spanName.text(pm.from.name);
        spanName.attr("usernameAuthor",pm.from.username);

        //Display the username of the author and its attrs are saved
        var spanUsername =  $("span.username",chirpElement);
        spanUsername.text("@" + pm.from.username);
        spanUsername.attr("usernameAuthor",pm.from.username);

        $("p",chirpElement).text(pm.message);
        $("date",chirpElement).text(pm.sent);

        var animate = $('.animate',chirpElement);
        //Detect youtube links in a chirp
        animate.youtube(chirpElement);

        //Detect emoticons in a chirp
        animate.anime();

        //Detect hashtags in a chirp
        animate.hashtagging(pm.hashtags,chirpElement);

        //Change style of unread messages
        if(pm.read === 0) {
              chirpElement.addClass("unreadPM");
        }
        else chirpElement.removeClass("unreadPM");

    }

    /* @author : Oscar
     * Paint the replies of a chirp in the Timetable
     * @param : reply[]: Stores the replies that will be displayed
                newReply: Boolean that says if it's new or not
                idOrigin: Id of the chirp of the origin of the conversation.
     * @return : Paint the replies of a chirp
     */
    function addReply(reply, newReply, idOrigin){
        if(newReply == false)
            var chirpElement = $("#chirpTemplate.template").clone().appendTo("#chirps").attr("class", "reply_template");
        else
            var chirpElement = $("#chirpTemplate.template").clone().prependTo("#chirps").attr("class", "reply_template");

        $("<div class=triangle></div>").insertBefore(chirpElement);
        var rechirpElement = $("#rechirp",chirpElement);
        var replyElement = $("#reply",chirpElement);
        var favoriteElement = $("#favorite",chirpElement);

        rechirpElement.attr("idChirp",reply.id);
        rechirpElement.attr("idChirper",reply.author.username);

        replyElement.attr("idChirp",idOrigin);
        replyElement.attr("idChirper",reply.author.username);

        favoriteElement.attr("idChirp",reply.id);
        favoriteElement.attr("idChirper",reply.author.username);

        if(reply.author.username != reply.chirper.username) {
            var spanRechirper = $("span.rechirper",chirpElement);
            spanRechirper.text("rechirped by @"+ reply.chirper.username);
            spanRechirper.attr("usernameRechirper",reply.chirper.username);
            chirpElement.css("background-color","#bfeeff");
            $('img.is_rechirp',chirpElement).show();
            spanRechirper.show();
        }

        //Load small avatar of the author of the chirp and its attrs are saved

        var smallAvatar = $(".small_avatar",chirpElement);
        smallAvatar.attr("src",reply.author.avatar);
        smallAvatar.attr("usernameAuthor",reply.author.username);
        smallAvatar.attr('class', 'very_small_avatar');

        //Display the name of the author and its attrs are saved
        var spanName =  $("span.name",chirpElement);
        spanName.text(reply.author.name);
        spanName.attr("usernameAuthor",reply.author.username);

        //Display the username of the author and its attrs are saved
        var spanUsername =  $("span.username",chirpElement);
        spanUsername.text("@" + reply.author.username);
        spanUsername.attr("usernameAuthor",reply.author.username);

        $("p",chirpElement).text(reply.message);
        $("date",chirpElement).text(reply.date);

        var animate = $('.animate',chirpElement);
        //Detect youtube links in a chirp
        animate.youtube(chirpElement);

        //Detect emoticons in a chirp
        animate.anime();

        //Detect hashtags in a chirp
        animate.hashtagging(reply.hashtags,chirpElement);

        if (reply.favorite) {
            event.preventDefault();

             favicon = $('#favTinyIcon',chirpElement);
             //console.log(favicon);
             is_favorited = chirpElement.children(".is_favorited");
             rechirp_as_favorited = chirpElement.children(".rechirp_is_favorited");
             is_rechirp = chirpElement.children(".is_rechirp");

             if(favicon.attr("src") == "img/favorite-icon.png") {
                 favicon.attr("src","img/favorite-icon2.png");
                 //$(this).css("color","#d47a02");
                 if(is_rechirp.is(":visible")) {
                    rechirp_as_favorited.css("display","block");
                    is_rechirp.css("display","none");
                 }
                 else is_favorited.css("display","block");
             }
             else {
                 favicon.attr("src","img/favorite-icon.png");
                 //$(this).css("color","")
                 if(rechirp_as_favorited.is(":visible")) {
                    rechirp_as_favorited.css("display","none");
                    is_rechirp.css("display","block");
                 }
                 else is_favorited.css("display","none");
             }
        }
        //getFavoriteChirps(chirpElement, reply.id);

    }

    function isLogged(){
       return localStorage["sessionID"];
    }

    /* @author : Oscar & Pere Joan
     * Render the user data (complete name, user, avatar and status) depending on your logged status (login or logout)
     * @param :
        sessionID: Contains all the useful data of a user and is empty if s/he is logged out
     * @return : Show the user data or not depending on his logged status
    */
    function renderUserData(){

        var myTimeline = $("#myTimeline");
        var ownTimeline = $("#ownTimeline");
        var chirp = $("#chirp");
        var follow = $("#follow");
        var unfollow = $("#unfollow");
        var profile = $("#profile");
        var ownfavorites = $('#ownfavorites');
        var privateMsg = $('#privateMsg');  //menu
        var sendPrivateMsg = $('#sendPrivateMsg');  //button

        //Unlogged user
        if(!isLogged()){
            $(".followForm").hide();
            $("#generalTimeline").removeClass("selected");

            myTimeline.hide();
            ownTimeline.hide();
            chirp.hide();
            follow.hide();
            unfollow.hide();
            profile.hide();
            ownfavorites.hide();
            privateMsg.hide();
            sendPrivateMsg.hide();
            //privateMessages.hide();
        }
        //Logged user
        else {
           $("#profileName").show();
           $("#profileName").text(localStorage["name"]);
           $(".myUserName").show();
           $(".myUserName").text("@" + localStorage["username"]);
           $(".status").show();
           $(".status").text(localStorage["status"]);
           $("#big_avatar").show();
           $("#big_avatar").attr("src",localStorage["avatar"]+"?s=120");
           $("#loggedAvatar").attr("src", localStorage["avatar"]);
           $("#logoutName").text("@" + localStorage["username"]);



           myTimeline.show();
           ownTimeline.show();
           chirp.show();
           follow.hide();
           unfollow.hide();
           profile.show();
           ownfavorites.show();
           privateMsg.show();
           sendPrivateMsg.hide();
           setNumberPM();
           //privateMessages.show();
        }
    }

    function setNumberPM(){
      var body = {
             sessionId : localStorage["sessionID"],
      };
      $.ajax({
          url: api.numPM,
          dataType: "json",
          data: JSON.stringify(body),
          type: "POST",
          contentType: "application/json; charset=utf-8",
          beforeSend: function (request)
          {
              request.setRequestHeader("credential", body.sessionId);
          },
          success: function(data,textStatus,jqXHR){
            $(".numberMP").text(data);
          },
          error: function(jqXHR, textStatus, error){
              if(jqXHR){
                  switch (jqXHR.status) {
                      case 400:
                             logoutSuccess();
                             flash(jqXHR.statusText+": "+jqXHR.responseText, "error");
                             break;
                         case 401:
                             logoutSuccess();
                             flash(jqXHR.statusText+": "+jqXHR.responseText, "error");
                             break;
                      case 404:
                          flash("Error logging in... api server not found at " + url, "error");
                          break;
                      default:
                           flash("Unknown error logging in", "error");
                  }
              }
          }
      });
    }

    /* @author : Pere Joan
     * Shows error or confirmation messages to end user in index.html
     * @param :
     *      error: Text that will be displayed
     *      type: <error/success> Sets if its an error message or success message.
     * @return : The user message is shown in index.html
     */
    function flash(error, type){
        $("<p>").addClass(type).text(error).prependTo("#messageErrorDiv").fadeOut(8000);
    };

    /* @author : Slodoban & Pere Joan
     * AJAX call to public a chirp if has the correct length and if there is a user logged in
     * @param :
        sessionID: Contains all the useful data of a user and is empty if s/he is logged out
     * @return : When the submit button is pushed, the new chirp is showed in he timeline.
    */
    function submitChirp(){
        var data = {
            sessionId : localStorage["sessionID"],
            message : document.getElementById("chirpMessage").value
        };
        if($.trim($('#chirpMessage').val()).length > 0){
            //localStorage["sessionID"] = '';
            $.ajax({
                url: api.newChirp,
                dataType: "json",
                data: JSON.stringify(data),
                type: "POST",
                contentType: "application/json; charset=utf-8",
                beforeSend: function (request)
                {
                    request.setRequestHeader("credential", data.sessionId);
                },
                success: function(data,textStatus,jqXHR){
                    //add chirp
                    if (localStorage["lastVisitedHashtag"]) lh = localStorage["lastVisitedHashtag"].substring(1);
                    if(!(localStorage["currentPage"] === timelineType.userT ||
                        (localStorage["currentPage"] === timelineType.hashtagT && (jQuery.inArray( lh, data.hashtags ) == -1))))
                                addChirp(data, true, false);

                    //remove text from chirpMessage
                    $('#chirpMessage').val('');
                    $('#counter').html('');
                },
                error: function(jqXHR, textStatus, error){
                    if(jqXHR){
                        switch (jqXHR.status) {
                            case 400:
                                logoutSuccess();
                                   flash(jqXHR.statusText+": "+jqXHR.responseText, "error");
                                   break;
                               case 401:
                                   logoutSuccess();
                                   flash(jqXHR.statusText+": "+jqXHR.responseText, "error");
                                   break;
                            case 404:
                                flash("Error logging in... api server not found at " + url, "error");
                                break;
                            default:
                                 flash("Unknown error logging in", "error");
                        }
                    }
                }
            });
        }else {
            //error message
            $('#chirpMessage').addClass('errorNotify');
            setTimeout(function(){
                $('#chirpMessage').removeClass('errorNotify');
            }, 1000);
        };
    };

    function submitReply(replyTo, textMessage){
            var data = {
                sessionId : localStorage["sessionID"],
                replyTo: replyTo,
                message : textMessage
            };
                $.ajax({
                    url: api.reply,
                    dataType: "json",
                    data: JSON.stringify(data),
                    type: "POST",
                    contentType: "application/json; charset=utf-8",
                    beforeSend: function (request)
                    {
                        request.setRequestHeader("credential", data.sessionId);
                    },
                    success: function(data,textStatus,jqXHR){
                        reloadPage();
                    },
                    error: function(jqXHR, textStatus, error){
                        if(jqXHR){
                            switch (jqXHR.status) {
                                case 400:
                                    logoutSuccess();
                                       flash(jqXHR.statusText+": "+jqXHR.responseText, "error");
                                       break;
                                   case 401:
                                       logoutSuccess();
                                       flash(jqXHR.statusText+": "+jqXHR.responseText, "error");
                                       break;
                                case 404:
                                    flash("Error logging in... api server not found at " + url, "error");
                                    break;
                                default:
                                     flash("Unknown error logging in", "error");
                            }
                        }
                    }
                });
        };

    function reloadPage() {
        if( isLogged() ) {
            $(".login").hide();
            $('#signup_lnk').hide();
            $(".logout").show();
            var body = { sessionId: localStorage["sessionID"] };
            console.log(localStorage["currentPage"])
            switch(localStorage["currentPage"])
            {
                case timelineType.generalT:
                $( "#generalTimeline" ).trigger( "click" );
                break;

                case timelineType.myT:
                $( "#myTimeline" ).trigger( "click" );
                break;

                case timelineType.ownT:
                $( "#ownTimeline" ).trigger( "click" );
                break;

                case timelineType.userT:

                userTimelineAJAXcall(localStorage["lastVisitedUser"],
                                     localStorage["lastVisitedUser-avatar"],
                                     localStorage["lastVisitedUser-name"],
                                     localStorage["lastVisitedUser-status"]);
                break;

                case timelineType.hashtagT:
                 var body = {
                      hashtag : localStorage["lastVisitedHashtag"]
                 };
                 getHashtags(body);
                break;

                case timelineType.myFav:
                $( "#ownfavorites" ).trigger( "click" );
                break;

                case timelineType.myPM:
                $( "#privateMsg" ).trigger( "click" );
                break;

                default:
                break;
            }
        }
        else {
            $("#generalTimeline" ).trigger( "click" );
            localStorage["currentPage"] = timelineType.generalT;
        }
        renderUserData();
    }

    function saveChirpers() {
        chirpers = [];
        for (chirp in chirps){
           chirpers[chirps[chirp].chirper.username]= chirps[chirp].chirper;
           if(chirps[chirp].author.username != chirps[chirp].chirper.username)
                chirpers[chirps[chirp].author.username]= chirps[chirp].author;
           for (reply in chirps[chirp].replies){
                chirpers[chirps[chirp].replies[reply].chirper.username] = chirps[chirp].replies[reply].chirper;
           }
        }
    }

    function saveChirpersPM(data) {
        chirpers = [];
        var messages = data;
        for (msg in messages){
           chirpers[messages[msg].from.username]= messages[msg].from;
        }
    }

    function renderTimeline (title, data) {
        $(".not_chirp_template").remove();
        $(".reply_template").remove();
        $(".triangle").remove();
        $(".chirpTemplateHeader").text(title);
        if(title === "General Timeline") $("#profile").hide();
        else {
            $("#profile").show();
            renderUserData();
        }
        chirps = data;
        saveChirpers();
        renderChirps();
        if(isLogged()) $('.chirpOptions').css('display','block');
        else $('.chirpOptions').css('display','none');
    }

    function renderPM (title, data) {
        $(".not_chirp_template").remove();
        $(".reply_template").remove();
        $(".triangle").remove();
        $(".chirpTemplateHeader").text(title);
        if(title === "General Timeline") $("#profile").hide();
        else {
            $("#profile").show();
            renderUserData();
        }
        PM = data;
        renderPMs();
        $('.chirpOptions').css('display','none');
    }

    function logoutSuccess () {
        $(".login").show();
        $('#signup_lnk').show();
        $(".logout").hide();
        $(".not_chirp_template").remove();
        $(".reply_template").remove();
        $(".triangle").remove();
        localStorage.clear();
        renderUserData();
        $("#loggedAvatar").attr("src", "#");
        $("#logoutName").text("");
        $( "#generalTimeline" ).trigger( "click" );
        //localStorage["lastVisitedHashtag"] = '';
        //localStorage["lastVisitedUser"] = '';
        //localStorage["lastVisitedUser-avatar"] = '';
        //localStorage["lastVisitedUser-name"] = '';
        //localStorage["lastVisitedUser-status"] = '';
    }

    function getFollowees (followee) {
          var body = {
                 sessionId : localStorage["sessionID"],
                 username : followee
          };
          $.ajax({
              url: api.followees,
              dataType: "json",
              data: JSON.stringify(body),
              type: "POST",
              contentType: "application/json; charset=utf-8",
              beforeSend: function (request)
              {
                  request.setRequestHeader("credential", body.sessionId);
              },
              success: function(data,textStatus,jqXHR){
                    $("#sendPrivateMsg").show();
                    $("#follow").show();
                    $("#unfollow").hide();
                    if(data.length != 0){
                        $.each( jQuery.unique(data), function( i, l ){
                            if(l.username == body.username)
                            {
                                $("#follow").hide();
                                $("#unfollow").show();
                            }
                        });
                    }
              },
              error: function(jqXHR, textStatus, error){
                  if(jqXHR){
                      switch (jqXHR.status) {
                          case 400:
                              logoutSuccess();
                                 flash(jqXHR.statusText+": "+jqXHR.responseText, "error");
                                 break;
                             case 401:
                                 logoutSuccess();
                                 flash(jqXHR.statusText+": "+jqXHR.responseText, "error");
                                 break;
                          case 404:
                              flash("Error logging in... api server not found at " + url, "error");
                              break;
                          default:
                               flash("Unknown error logging in", "error");
                      }
                  }
              }
          });
    }

    /*function getFavoriteChirps (chirpElement, chirpId, show) {
        if(isLogged() != undefined){
          var body = {
                 sessionId : localStorage["sessionID"],
          };
          $.ajax({
              url: api.favorites,
              dataType: "json",
              data: JSON.stringify(body),
              type: "POST",
              contentType: "application/json; charset=utf-8",
              beforeSend: function (request)
              {
                  request.setRequestHeader("credential", body.sessionId);
              },
              success: function(data,textStatus,jqXHR){
                  console.log(data);
                  *//*for (var key in data) {
                    if(chirpId == data[key].id){
                      if(data[key].author.username != data[key].chirper.username) {
                        
                        $('.is_rechirp',chirpElement).css('display','none');
                        $('.rechirp_is_favorited',chirpElement).addClass('favorite').css('display','block');
                        $('#favorite img', chirpElement).attr('src','img/favorite-icon2.png');
                      }else{
                        $('.is_favorited',chirpElement).addClass('favorite').css('display','block');
                        $('#favorite img', chirpElement).attr('src','img/favorite-icon2.png');
                      }
                    }
                  };*//*
              },
              error: function(jqXHR, textStatus, error){
                  if(jqXHR){
                      switch (jqXHR.status) {
                          case 400:
                                 flash(jqXHR.statusText+": "+jqXHR.responseText, "error");
                                 break;
                             case 401:
                                 flash(jqXHR.statusText+": "+jqXHR.responseText, "error");
                                 break;
                          case 404:
                              flash("Error logging in... api server not found at " + url, "error");
                              break;
                          default:
                               flash("Unknown error logging in", "error");
                      }
                  }
              }
          });
        }
    }*/

    function checkCounter(sendButton, counter, count, length) {
        //console.log (sendButton+" " + counter + " " + count +" " +length);
        if((count != 141) && (count >= 0)){
                    sendButton.attr("disabled", false);
                    if (event.which == 13) {
                        submitChirp();
                        sendButton.attr("disabled", true);
                    };
        }
        else sendButton.attr("disabled", true);

        if(count < 0) counter.addClass('tooLong');
        else counter.removeClass('tooLong');
        if (count == 141) count ="";
        counter.html(count);
    }

    function getHashtags(body) {
        localStorage["lastVisitedHashtag"] = body.hashtag;
          $.ajax({
               url: api.hashtag,
               dataType: "json",
               data: JSON.stringify(body),
               type: "POST",
               success: function(data, textStatus, jqXHR){
                  $('.main').each(function(){
                      $(this).children().removeClass("selected");
                  });
                  renderTimeline("Chirps with "+body.hashtag, data);
                  localStorage["currentPage"] = timelineType.hashtagT;
               },
               error: function(jqXHR, textStatus, error){
                   switch (jqXHR.status) {
                      case 400:
                           logoutSuccess();
                           flash(jqXHR.statusText+": "+jqXHR.responseText, "error");
                           break;
                       case 401:
                           logoutSuccess();
                           flash(jqXHR.statusText+": "+jqXHR.responseText, "error");
                           break;
                      case 404:
                          flash("Error getting the chirps of the hashtag "+body.hashtag+". API server not found at " + url, "error");
                          break;
                      default:
                           flash("Unknown error logging in", "error");
                    }
                }
           });
    }

    function userTimelineAJAXcall (username, avatar, name, status) {

        var body = {
                         username : username,
                         avatar: avatar,
                         name: name,
                         status: status
        };

        localStorage["lastVisitedUser"] = username;
        localStorage["lastVisitedUser-avatar"] = avatar;
        localStorage["lastVisitedUser-name"] = name;
        localStorage["lastVisitedUser-status"] = status;

        $.ajax({
             url: api.userTimeline,
             dataType: "json",
             data: JSON.stringify(body),
             type: "POST",
             beforeSend: function (request)
             {
                 if(isLogged()){
                     request.setRequestHeader("Session", localStorage["sessionID"]);
                 }
             },
             success: function(data,textStatus,jqXHR){

                 //If the request user is yourself, he displayed text is 'My Chirps'
                 if(body.username.replace(/\s/g, '') == localStorage["username"]) {
                     $( "#myTimeline" ).trigger( "click" );
                     localStorage["currentPage"] = timelineType.ownT;
                 }
                 else {
                     $("#chirps li.not_chirp_template").remove();
                     $("#chirps li.reply_template").remove();
                     $("#chirps div.triangle").remove();
                     $("#profile").show();
                     $("#content h1 div").text("Chirps by "+ body.username);
                     $("header h1").text(name);
                     $("header h2").text('@'+username);
                     $("header p.status").text(status);
                     $("#big_avatar").attr("src",avatar+"?s=120");
                     $('.main').each(function(){
                         $(this).children().removeClass("selected");
                     });
                     localStorage["currentPage"] = timelineType.userT;

                     //Load Follow/Unfollow button
                     if(isLogged()) getFollowees(username);

                      chirps = data;
                      saveChirpers();
                      renderChirps();
                 }
             },
             error: function(jqXHR, textStatus, error){
                 switch (jqXHR.status) {
                   case 400:
                        logoutSuccess();
                        flash(jqXHR.statusText+": "+jqXHR.responseText, "error");
                        break;
                    case 401:
                        logoutSuccess();
                        flash(jqXHR.statusText+": "+jqXHR.responseText, "error");
                        break;
                   case 404:
                       flash("Error getting the chirps of the selected user... api server not found at " + url, "error");
                       break;
                   default:
                        flash("Unknown error logging in", "error");
                 }
             }
         });
    }



/***********************************************************************************************************************
*                                                                                                                      *
*           Beggining of the CODE                                                                                      *
*                                                                                                                      *
***********************************************************************************************************************/

    $(function(){

        /* @author : Pere Joan
         * Displays the main page depending on he user status (logged in or logged out)
         * @param : -
         * @return : Hides the options of a logged user if it's not logged in
         */

         //Setting the default current page the first time you
         if (typeof localStorage["currentPage"] === "undefined") localStorage["currentPage"] = timelineType.generalT;
         $(function(){
            reloadPage();
         });

/***********************************************************************************************************************
*                                                                                                                      *
*           Changing-style functions                                                                                   *
*                                                                                                                      *
***********************************************************************************************************************/

             $(".main a").on('click', function(){
                  $(".main .selected").removeAttr( "class" );
                  $(this).addClass("selected");
             });

             //Changing Style of Unfollow button
              $("#unfollow")
                 .mouseenter(function(){
                     $(this).attr("value","Unfollow");
                 })
                 .mouseleave(function(){
                      $(this).attr("value","Following");
                 });

              //Changing Style of Favorite button
              $("#favorite").live('click',function(e) {
                 event.preventDefault();

                 favicon = $(this).children();
                 var selectedChirp = $(this).parents('li#chirpTemplate');
                 if(selectedChirp.attr('class') === "reply_template") {
                     is_favorited = selectedChirp.children(".is_favorited");
                     rechirp_as_favorited = selectedChirp.children(".rechirp_is_favorited");
                     is_rechirp = selectedChirp.children(".is_rechirp");
                 }
                 else {
                    is_favorited = selectedChirp.children(".is_favorited");
                    rechirp_as_favorited = selectedChirp.children(".rechirp_is_favorited");
                    is_rechirp = selectedChirp.children(".is_rechirp");
                 }


                 if(favicon.attr("src") == "img/favorite-icon.png") {
                     favicon.attr("src","img/favorite-icon2.png");
                     $(this).css("color","#d47a02");
                     if(is_rechirp.is(":visible")) {
                        rechirp_as_favorited.css("display","block");
                        is_rechirp.css("display","none");
                     }
                     else is_favorited.css("display","block");
                 }
                 else {
                     favicon.attr("src","img/favorite-icon.png");
                     $(this).css("color","")
                     if(rechirp_as_favorited.is(":visible")) {
                        rechirp_as_favorited.css("display","none");
                        is_rechirp.css("display","block");
                     }
                     else is_favorited.css("display","none");
                 }
              });

              //Create a reply of a CHIRP
              $("#reply").live('click',function(e) {
                   event.preventDefault();
                   var replySection = $(this).parents('li#chirpTemplate').children('.replyChirp');
                   replySection.css('display','block');
                   $('.replyTextArea').focus();
                   $('.replyTextArea').val( "@"+$(this).attr('idChirper'));
                   var max = 141;
                   var length = $.trim($('.replyTextArea').val()).length
                   checkCounter($('.replyChirpButton'), $('.counterReply'), max - length, length);
                   $(this).parent().css('display','block');
                   //console.log($(this).attr('idChirp'));
               });

             //Loading screen
             $("body").on({
                 ajaxStart: function() {
                     $(this).addClass("loading");
                     $("videoframe").hide();
                 },
                 ajaxStop: function() {
                     $(this).removeClass("loading");
                     $("videoframe").show();
                 }
             });




/***********************************************************************************************************************
*                                                                                                                      *
*           Event-listeners functions + AJAX calls                                                                     *
*                                                                                                                      *
***********************************************************************************************************************/

         /**************************************************************************************************************
         *           Login/Logout                                                                                      *
         **************************************************************************************************************/

        /* @author : Pere Joan
         * Login function to be able to start a session as an authenticated
         * @param : username: String
                    password: String
         * @return : Error messages if something is wrong or success message if everything is fine.
         *           Timeline and user data is shown
         */
         $('.login').submit(function(e){
            e.preventDefault();
            var body = {
                username : $("#username").val(),
                password : $("#password1").val()
            };
            $.ajax({
                url: api.login,
                dataType: "json",
                data: JSON.stringify(body),
                success: function(data){
                      $(".login").hide();
                      //Delete the content of the username and password login fields
                      $('.login').each(function(){ this.reset() });
                      $('#signup_lnk').hide();
                      $(".logout").show();

                      /* Storing user data */
                      if (Modernizr.localstorage) {
                          localStorage["sessionID"]              = data.id;
                          localStorage["userID"]                 = data.userData.id;
                          localStorage["name"]                   = data.userData.name;
                          localStorage["username"]               = data.userData.username;
                          localStorage["avatar"]                 = data.userData.avatar;
                          localStorage["status"]                 = data.userData.status;
                          localStorage["currentPage"]            = timelineType.myT;
                          localStorage["lastVisitedUser"]        = '';
                          localStorage["lastVisitedUser-avatar"] = '';
                          localStorage["lastVisitedUser-name"]   = '';
                          localStorage["lastVisitedUser-status"] = '';
                          localStorage["lastVisitedHashtag"]     = '';
                          //favoriteChirps = data.favorites;
                      } else {
                          flash("No native support for HTML5 storage :(", "error")
                      }

                      /* load My Timeline */
                      var body = { sessionId: data.id };
                      $( "#myTimeline" ).trigger( "click" );
                      renderUserData();
                      flash("Welcome @" + data.userData.username + " !", "success");
                },
                type: "POST",
                error: function(jqXHR, textStatus, error){
                    if(jqXHR){
                        switch (jqXHR.status) {
                            case 400:
                                logoutSuccess();
                                   flash(jqXHR.statusText+": "+jqXHR.responseText, "error");
                                   break;
                               case 401:
                                   logoutSuccess();
                                   flash(jqXHR.statusText+": "+jqXHR.responseText, "error");
                                   break;
                            case 404:
                                flash("Error logging in... api server not found at " + url, "error");
                                break;
                            default:
                                 flash("Unknown error logging in", "error");
                        }
                    }
                }
            });
         });
        
        /* @author : Slobodan
         * ajax JSON call after submit is done
         * @param : author (username), string
         * @param : date (date()) , date
         * @param : message (chirp message), string
         * @return : success add new chirp  / error alert
         */
        $('#logout').on("click", function(e){
            e.preventDefault();
            var body = { sessionId: localStorage["sessionID"] };
            $.ajax({
                url: api.logout,
                data: JSON.stringify(body),
                beforeSend: function (request)
                {
                    request.setRequestHeader("Session", body.sessionId);
                },
                success: function(data,textStatus,jqXHR){
                    logoutSuccess();
                },
                dataType: "json",
                type: "POST",
                error: function(jqXHR, textStatus, error){
                    if(jqXHR){
                        switch (jqXHR.status) {
                            case 400:
                               logoutSuccess();
                               flash(jqXHR.statusText+": "+jqXHR.responseText, "error");
                               break;
                           case 401:
                               logoutSuccess();
                               flash(jqXHR.statusText+": "+jqXHR.responseText, "error");
                               break;
                            case 404:
                                flash("Error logging out... api server not found at " + api.chirps, "error");
                                break;
                            default:
                                 flash("Unknown error logging in", "error");
                        }
                    }
                }
            });
        });

        /***************************************************************************************************************
        *           Chirp Message                                                                                      *
        ***************************************************************************************************************/

        /* @author : Slobodan
         * Chirping
         */

        $('#newChirp').on('click',function(e){
            e.preventDefault();
            submitChirp();
        });

         $('.replyChirpButton').live('click',function(e){
             e.preventDefault();
             //flash("Not implemented yet ^^","error");
             var replyMessage = $(this).parent().prev().val();
             var replyTo = $(this).parent().parent().parent().prev().children().first().attr('idChirp');
             submitReply(replyTo, replyMessage);
         });

        /* @author : Slobodan
         * ajax JSON call after submit is done
         * @param : author (username), string
         * @param : date (date()) , date
         * @param : message (chirp message), string
         * @return : success add new chirp  / error alert
         */
        //$('#chirpMessage, .replyTextArea, .privateMsgArea').live("keydown, keyup", function( event ){
        $('#chirpMessage, .replyTextArea, .privateMsgArea').live("input propertychange", function( event ){
            msg = $(this).val();
            var length = $.trim(msg).length;
            var max = 141;
            var tce = [":)", ":0", ";)", ":D", ":(", ";D", ":P", ":O", ":s", ":|", ":*" ];
            RegExp.escape= function(s) {
                return s.replace(/[-\/\\^$*+?.()|[\]{}]/g, '\\$&')
            }
            var re = RegExp(tce.map(RegExp.escape).join('|'), "g");
            var countOcurrences = (msg.match(re) || "").length;
            var  count = max - msg.length + countOcurrences;

            if($(this).attr('id') == 'chirpMessage') {
                var sendButton = $("#newChirp");
                var counter =  $('#counter');
            }
            else if ($(this).attr('class') == 'privateMsgArea') {
                var sendButton = $(".sendPrivateMessageButton");
                var counter =  $('.counterPM');
            }
            else {
                var sendButton = $(".replyChirpButton");
                var counter = $(".counterReply");
            }

            checkCounter(sendButton, counter, count, length);

        });

        $('.replyTextArea').live("blur", function( event ){
            event.preventDefault();
            setTimeout(function() {
                  $('.replyTextArea').val("");
                  $('.counterReply').val("");
                  $('.replyTextArea').parents('.replyChirp').css('display','none');
            }, 1000);
        });

         /**************************************************************************************************************
         *           Timelines                                                                                        *
         **************************************************************************************************************/

        /* @author : Oscar
         * AJAX call to load the chirps of my followings
        */
        $("#myTimeline").on('click',function(e){
            var body = { sessionId: localStorage["sessionID"] };
            $.ajax({
                 url: api.myTimeline,
                 dataType: "json",
                 data: JSON.stringify(body),
                 type: "POST",
                 beforeSend: function (request)
                 {
                     request.setRequestHeader("Session", body.sessionId);
                 },
                 success: function(data,textStatus,jqXHR){
                     renderTimeline("My Timeline", data);
                     localStorage["currentPage"] = timelineType.myT;
                 },
                 error: function(jqXHR, textStatus, error){
                     switch (jqXHR.status) {
                         case 400:
                             logoutSuccess();
                             flash(jqXHR.statusText+": "+jqXHR.responseText, "error");
                             break;
                         case 401:
                             logoutSuccess();
                             flash(jqXHR.statusText+": "+jqXHR.responseText, "error");
                             break;
                         case 404:
                             flash("Error getting the chirps of your timeline... api server not found at " + url, "error");
                             break;
                         default:
                              flash("Unknown error logging in", "error");
                     }
                 }
             });
        });

        /* @author : Oscar
         * AJAX call to load my own chirps
         * @param : id (of the user), string
         * @return :
            success: Show the chirps of mine sorted by date
            error: Alert message saying the kind of error
        */
        $("#ownTimeline").on('click',function(e){
            var body = { sessionId: localStorage["sessionID"] };
             $.ajax({
                 url: api.ownTimeline,
                 dataType: "json",
                 data: JSON.stringify(body),
                 type: "POST",
                 beforeSend: function (request)
                 {
                     request.setRequestHeader("Session", body.sessionId);
                 },
                 success: function(data,textStatus,jqXHR){
                     renderTimeline("My Chirps", data);
                     localStorage["currentPage"] = timelineType.ownT;
                 },
                 error: function(jqXHR, textStatus, error){
                     switch (jqXHR.status) {
                          case 400:
                               logoutSuccess();
                               flash(jqXHR.statusText+": "+jqXHR.responseText, "error");
                               break;
                           case 401:
                               logoutSuccess();
                               flash(jqXHR.statusText+": "+jqXHR.responseText, "error");
                               break;
                          case 404:
                              flash("Error getting your chirps... api server not found at " + url, "error");
                              break;
                          default:
                               flash("Unknown error logging in", "error");
                      }
                  }
             });
         });

        /* @author : Oscar
         * AJAX call to load my own chirps
         * @param : id (of the user), string
         * @return :
            success: Show the chirps of mine sorted by date
            error: Alert message saying the kind of error
        */
        $("#ownfavorites").on('click',function(e){
            var body = { sessionId: localStorage["sessionID"] };
             $.ajax({
                 url: api.favorites,
                 dataType: "json",
                 data: JSON.stringify(body),
                 type: "POST",
                 beforeSend: function (request)
                 {
                    request.setRequestHeader("Session", body.sessionId);
                 },
                 success: function(data,textStatus,jqXHR){
                     favoriteChirps = data;
                     renderTimeline("My Favorites", favoriteChirps);
                     localStorage["currentPage"] = timelineType.myFav;
                 },
                 error: function(jqXHR, textStatus, error){
                     switch (jqXHR.status) {
                          case 400:
                               logoutSuccess();
                               flash(jqXHR.statusText+": "+jqXHR.responseText, "error");
                               break;
                           case 401:
                               logoutSuccess();
                               flash(jqXHR.statusText+": "+jqXHR.responseText, "error");
                               break;
                          case 404:
                              flash("Error getting your chirps... api server not found at " + url, "error");
                              break;
                          default:
                               flash("Unknown error logging in", "error");
                      }
                  }
             });
         });

         $('#generalTimeline').on('click',function(e){
            var body = { sessionId: localStorage["sessionID"] };
             $.ajax({
                url: api.chirps,
                beforeSend: function (request)
                 {
                    if(body.sessionId)
                        request.setRequestHeader("Session", body.sessionId);
                 },
                success: function(newChirps,textStatus,jqXHR){
                    //console.log(newChirps);
                    renderTimeline("General Timeline", newChirps);
                    localStorage["currentPage"] = timelineType.generalT;
                 },
                 dataType: "json",

                 error: function(jqXHR, textStatus, error){
                    switch (jqXHR.status) {
                      case 400:
                           logoutSuccess();
                           flash(jqXHR.statusText+": "+jqXHR.responseText, "error");
                           break;
                       case 401:
                           logoutSuccess();
                           flash(jqXHR.statusText+": "+jqXHR.responseText, "error");
                           break;
                      case 404:
                          flash("Error getting the chirps... api server not found at " + url, "error");
                          break;
                      default:
                           flash("Unknown error logging in", "error");
                    }
                }
             });
         });


         /* @author : Oscar , Slobodan
          * AJAX call to load the chirps of a user
          * @param : id (of the user), string
          * @return :
             success: Show the chirps of the selected user
             error: Alert message saying the kind of error
         */
         $('.rechirper, .username, .name, .small_avatar, .userMention').live('click',function(e){
             //load the timeline of another user
             switch($(this).attr('class')){

                 case "rechirper":
                     username = $(this).attr('usernameRechirper');
                     break;

                 default :
                     username = $(this).attr("usernameAuthor");
                     break;
             }
             if(!chirpers[username])
                 flash("Error: The user @"+username+" doesn't exist.", "error");
             else {
                     avatar = chirpers[username].avatar;
                     name = chirpers[username].name;
                     status = chirpers[username].status;
                     userTimelineAJAXcall(username, avatar, name, status);
             }
         });

         /**************************************************************************************************************
         *           Hashtag                                                                                           *
         **************************************************************************************************************/

         $("#hash").live('click',function(e){
              var body = {
                     hashtag : $(this).text()
              };
              getHashtags(body);
          });

         /**************************************************************************************************************
         *           Follow/unfollow                                                                                   *
         **************************************************************************************************************/

         $("#follow, #unfollow").live('click', function(){
              //load the timeline of another user
              var action = $(this).attr('id');
              switch(action){
                  case "follow":
                     url = api.follow;
                     break;
                  case "unfollow":
                     url = api.unfollow;
                     break;
                  default :
                     break;
              }

              var user_info = {
                  follower : $('.myUserName').text().substring(1),
                  sessionId : localStorage["sessionID"],
                  sessionUsername : localStorage["username"],
                  url : url
              };

              if(user_info.follower != user_info.sessionUsername){
                  $.ajax({
                      url: user_info.url,
                      dataType: "json",
                      data: JSON.stringify(user_info),
                      type: "POST",
                      beforeSend: function (request)
                      {
                          request.setRequestHeader("Session", user_info.sessionId);
                      },
                      success: function(data,textStatus,jqXHR){
                          if(url == api.unfollow){
                              $('#unfollow').hide();
                              $('#follow').show();
                          }
                          else{
                              $('#unfollow').show();
                              $('#follow').hide();
                          }
                      },
                      error: function(jqXHR, textStatus, error){
                          switch (jqXHR.status) {
                            case 400:
                               logoutSuccess();
                               flash(jqXHR.statusText+": "+jqXHR.responseText, "error");
                               break;
                            case 401:
                               logoutSuccess();
                               flash(jqXHR.statusText+": "+jqXHR.responseText, "error");
                               break;
                            case 404:
                                flash("Error getting the chirps of the selected user... api server not found at " + url, "error");
                                break;
                            default:
                                 flash("Unknown error logging in", "error");
                          }
                      }
                 });
              }
          });
        
         /**************************************************************************************************************
         *           Favorite / Unfavorite                                                                                       *
         **************************************************************************************************************/
         $('#favorite').live('click',function(e) {
            var chirp = $(this).parent().parent();

            favicon = $(this).children();
            if(favicon.attr("src") == "img/favorite-icon.png") {
              var apiFavoriteChirp = api.unfavorite;
            }
            else{
              var apiFavoriteChirp = api.favorite;
            }

            var data = {
                  sessionId : localStorage["sessionID"],
                  favorite : $(this).attr('idChirp')
            };

            $.ajax({
                  url: apiFavoriteChirp,
                  dataType: "json",
                  data: JSON.stringify(data),
                  type: "POST",
                  beforeSend: function (request)
                  {
                      request.setRequestHeader("Session", data.sessionId);
                  },
                  success: function(data, textStatus, jqXHR) {
                    if(localStorage["currentPage"] === timelineType.myFav && apiFavoriteChirp === api.unfavorite) {
                        reloadPage();
                    }
                  },
                  error: function(jqXHR, textStatus, error) {
                      switch (jqXHR.status) {
                          case 400:
                             logoutSuccess();
                             flash(jqXHR.statusText+": "+jqXHR.responseText, "error");
                             break;
                          case 401:
                             logoutSuccess();
                             flash(jqXHR.statusText+": "+jqXHR.responseText, "error");
                             break;
                          case 404:
                              flash("Error publishing your chirp... api server not found at "+ url, "error");
                              break;
                          default:
                               flash("Unknown error logging in", "error");
                        }
                  }
              });
         });


         /**************************************************************************************************************
         *           Rechirp                                                                                           *
         **************************************************************************************************************/

         $('#rechirp').live('click',function(e) {
            if($(this).attr("idChirper") != localStorage["username"])
            {
                var data = {
                    sessionId : localStorage["sessionID"],
                    chirpId : $(this).attr('idChirp')
                };
                $.ajax({
                    url: api.rechirp,
                    dataType: "json",
                    data: JSON.stringify(data),
                    type: "POST",
                    beforeSend: function (request)
                    {
                        request.setRequestHeader("Session", data.sessionId);
                    },
                    success: function(data, textStatus, jqXHR) {
                         addChirp(data, true);
                    },
                    error: function(jqXHR, textStatus, error) {
                        switch (jqXHR.status) {
                            case 400:
                               logoutSuccess();
                               flash(jqXHR.statusText+": "+jqXHR.responseText, "error");
                               break;
                            case 401:
                               logoutSuccess();
                               flash(jqXHR.statusText+": "+jqXHR.responseText, "error");
                               break;
                            case 404:
                                flash("Error publishing your chirp... api server not found at "+ url, "error");
                                break;
                            default:
                                 flash("Unknown error logging in", "error");
                          }
                    }
                });
            }
            else {
                //event.preventDefault();
                flash("You can't rechirp a chirp of yourself!", "error");
            }
         });

        /**************************************************************************************************************
        *           Send Private Message                                                                                           *
        **************************************************************************************************************/
         $("#privateMsg").live('click',function(e){
              var body = { sessionId: localStorage["sessionID"] };
              $.ajax({
                   url: api.privateMessages,
                   dataType: "json",
                   data: JSON.stringify(body),
                   type: "POST",
                   success: function(data, textStatus, jqXHR){
                      renderPM("My Private Messages", data);
                      localStorage["currentPage"] = timelineType.myPM;
                      saveChirpersPM(data);
                   },
                   error: function(jqXHR, textStatus, error){
                       switch (jqXHR.status) {
                          case 400:
                               logoutSuccess();
                               flash(jqXHR.statusText+": "+jqXHR.responseText, "error");
                               break;
                           case 401:
                               logoutSuccess();
                               flash(jqXHR.statusText+": "+jqXHR.responseText, "error");
                               break;
                          case 404:
                              flash("Error getting your private messages. API server not found at " + url, "error");
                              break;
                          default:
                               flash("Unknown error logging in", "error");
                        }
                    }
               });
          });

        $('#sendPrivateMsg').live('click',function(e) {;
            var olay = $('div.overlay');
            var modals = $('div.modal2');
            var currentModal;
            var isopen=false;

            olay.css({opacity : 0});
            olay.hide();
            modals.hide();
            e.preventDefault();
            currentModal = $('.modal2');
            openModal();

            function openModal(){
                modals.hide();
                currentModal.css({
                    top:$(window).height() /2 - currentModal.outerHeight() /2 + $(window).scrollTop(),
                    left:$(window).width() /2 - currentModal.outerWidth() /2 + $(window).scrollLeft()
                });

                if(isopen===false){
                    olay.css({opacity :  0.7, backgroundColor: '#a2d3cd'});
                    olay['fadeIn'](400);
                    currentModal.delay(400)['fadeIn'](400);
                }else{
                    currentModal.show();
                }
                isopen=true;
            }

            function closeModal(){
                isopen=false;
                modals.fadeOut(100, function(){
                    olay.fadeOut();
                });

                return false;
            }

            $('#sendPM').unbind().on('click', function(e) {
                var user = $('.myUserName').text().substring(1);
                var text = $("#area").val();
                var data = {
                    sessionId: localStorage["sessionID"],
                    message: text,
                    to:user
                };
                console.log("sendMessage: "+text)
                $.ajax({
                    url: api.send,
                    dataType: "json",
                    data: JSON.stringify(data),
                    type: "POST",
                    beforeSend: function (request)
                    {
                        request.setRequestHeader("Session", data.sessionId);
                    },
                    success: function(data, textStatus, jqXHR) {

                        $("#area").val("");
                       closeModal();
                    },
                    error: function(jqXHR, textStatus, error) {
                        flash("Unkown error", error);
                    }
                });

            })


            olay.bind('click', closeModal);
            $('.closeBtn').bind('click', closeModal);
            $(window).bind('keyup', function(e){
                if(e.which === 27){
                    closeModal();
                }
             });
        });
    });
})();