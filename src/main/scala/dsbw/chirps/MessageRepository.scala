package dsbw.chirps

import dsbw.mongo.MongoDao
import org.bson.types.ObjectId
import java.util.Date
import com.mongodb.casbah.commons.MongoDBObject
import com.novus.salat.dao.SalatMongoCursor

//import com.mongodb.casbah.Imports._
import com.mongodb.casbah.query.Imports._


/** A record representing the scheme of Private Messages stored in the PM collection */
//
case class MessageRecord(from:ObjectId,to:ObjectId, message:String, read:Int, sent:Date=new Date(),_id:ObjectId = new ObjectId())

/** PMs Data Access Object */
class MessageDao(db:DB) extends MongoDao[MessageRecord](db.messages) {
}

class MessageRepository(dao:MessageDao) {

  private val sortCriteria = MongoDBObject("sent" -> -1)

  /* public functions*/
  def read(id:ObjectId):List[MessageRecord] = {
    val msgs = dao.find(Map[String, (AnyRef)]("to"->id)).sort(sortCriteria).toList
    for(msg <- msgs) {
      val read = new Object()
      dao.updateParamByID2(msg._id,"read",1)
    }
    return msgs
  }


  def send(id:ObjectId, to: ObjectId, message:String) {
    val msg = new MessageRecord(id,to, message,0)
    dao.save(msg)
  }

  def numPM(id:ObjectId):Int = {
    val msgs = dao.find(Map[String, (AnyRef)]("to"->id)).sort(sortCriteria).toList
    return msgs.length
  }

   println(read(new ObjectId("52a895d244ae86b16be17036")))
}


