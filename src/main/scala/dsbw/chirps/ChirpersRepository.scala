package dsbw.chirps

import org.bson.types.ObjectId
import dsbw.mongo.MongoDao
import com.mongodb.casbah.commons.MongoDBObject

/** Case class that documents the document scheme in the chirper MongoDB collection
  * @param _id: Chirper ID
  * @param username: Unique username
  * @param name: Real user Name
  * @param avatar: Path to the avatar
  * @param status: User status
  * @param age: User age in years
  * @param followers: Sequence of followers ID
  * @param followings: Sequence of followings ID
  * @param favorites: Sequence of favorites ID
  * @param password: Password store as result of Hash (SHA-256)
  **/
case class ChirperRecord(username:String, name:String, avatar:String, status: String, age: Int, password: Seq[Byte],
                         followers: Seq[ObjectId] = Seq(), followings: Seq[ObjectId]  = Seq(),
                         favorites: Seq[ObjectId] = Seq(), _id:ObjectId = new ObjectId())

class ChirpersDao(db:DB) extends MongoDao[ChirperRecord](db.chirpers) {
}

class ChirpersRepository(dao: ChirpersDao) {

  def findById(id:ObjectId) = dao.findOneByID(id)

  def findByUsername(username: String) = dao.findOneByParam("username",username)

  def saveChirper(chirper:ChirperRecord) = {
    dao.save(chirper)
  }

  def saveChirper(username: String, name: String, avatar: String, status: String, age: Int, password: Seq[Byte]) = {
    var u = new ChirperRecord(username, name, avatar, status, age, password)
    dao.save(u)
  }

  def getFollowers(userId:ObjectId):Seq[ObjectId] = {
    val user = findById(userId)
    return user.get.followers
  }

  def getFollowings(userId:ObjectId):Seq[ObjectId] = {
    val user = findById(userId)
    return user.get.followings
  }

  def getFavorites(userId:ObjectId):Seq[ObjectId] = {
    val user = findById(userId)
    return user.get.favorites
  }


  def addFollow(userId:ObjectId, followId:ObjectId) = {
    var user = findById(userId).get.copy()
    var followings = user.followings ++ Seq(followId)
    dao.updateParamByID(userId,"followings",followings)
  }

  def removeFollow(userId:ObjectId, followId:ObjectId) = {
    var user = findById(userId).get.copy()
    var followings = user.followings.diff(Seq(followId))
    dao.updateParamByID(userId,"followings",followings)
  }

  def addFollower(userId:ObjectId, followerId:ObjectId) = {
    var user = findById(userId).get.copy()
    var followers = user.followers ++ Seq(followerId)
    dao.updateParamByID(userId,"followers",followers)
  }

  def removeFollower(userId:ObjectId, followerId:ObjectId) = {
    var user = findById(userId).get.copy()
    var followers = user.followers.diff(Seq(followerId))
    dao.updateParamByID(userId,"followers",followers)
  }

  def addFavorite(userId:ObjectId, favorite:ObjectId) = {
    var user = findById(userId).get.copy()
    var favorites = user.favorites ++ Seq(favorite)
    dao.updateParamByID(userId,"favorites",favorites)
  }

  def removeFavorite(userId:ObjectId, favorite:ObjectId) = {
    var user = findById(userId).get.copy()
    var favorites = user.favorites.diff(Seq(favorite))
    dao.updateParamByID(userId,"favorites",favorites)
  }

  // def findByIds(ids:Array[ObjectId]) = dao.findByIds(ids:Set[ObjectId])
  //dao.save(ChirperRecord(username="agile_jordi", name="Jordi Pradel", avatar="/img/avatar4.png"))

}