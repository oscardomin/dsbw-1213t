package dsbw.chirps

import dsbw.mongo.MongoDao
import org.bson.types.ObjectId
import java.util.Date
import com.mongodb.casbah.commons.MongoDBObject
import com.novus.salat.dao.SalatMongoCursor

//import com.mongodb.casbah.Imports._
import com.mongodb.casbah.query.Imports._

/** A record representing the scheme of Chirps stored in the chirps collection */
//
case class ChirpRecord(chirper:ObjectId, author:ObjectId, message:String, hashtags:Seq[String],
                       replyTo:ObjectId = null, replies:Seq[ObjectId] = Seq(),
                       date:Date = new Date(), _id:ObjectId = new ObjectId())

/** Chirps Data Access Object */
class ChirpsDao(db:DB) extends MongoDao[ChirpRecord](db.chirps) {
}

/** Chirps Repository
  * A repository uses a DAO but doesn't expose all the DB centric API of the DAO
  */
class ChirpsRepository(dao: ChirpsDao) {

  private val sortCriteria = MongoDBObject("date" -> -1)

  /* Find Set */
  
  def findAll:List[ChirpRecord] =
    dao.findAll.sort(sortCriteria).toList

  def findUserChirps(id:ObjectId):List[ChirpRecord] =
    dao.find(Map[String, (AnyRef)]("chirper"->id)).sort(sortCriteria).toList

  //def findFolloweesTimeline(ids:Array[ObjectId]):List[ChirpRecord] =
  def findUsersChirps(ids:Seq[ObjectId]):List[ChirpRecord] =
    dao.findSubQuery("chirper",ids.toSet).sort(sortCriteria).toList

  def findByHashtag(hashtag:String):List[ChirpRecord] =
    dao.findByParam("hashtags",hashtag).sort(sortCriteria).toList
  
  /*def incRechirps(idChirp:String) {
    dao.updateById(new ObjectId(idChirp), $inc("rechirpsCount"->1))
  }*/

  def findById(id:ObjectId) = dao.findOneByID(id)

  def saveChirp(chirp:ChirpRecord) {
    dao.save(chirp)
  }

  def addReply(chirpId:ObjectId, reply:ObjectId) = {
    var chirp = findById(chirpId).get.copy()
    var replies = chirp.replies ++ Seq(reply)
    dao.updateParamByID(chirpId,"replies",replies)
  }

  /*def saveChirp(ChirperId: ObjectId, AuthorId:ObjectId, message:String, hashtags:Seq[String], chirpId:ObjectId) {
    var c = new ChirpRecord(ChirperId,AuthorId, message, hashtags, _id=chirpId);
    dao.save(c);
  }*/
}
