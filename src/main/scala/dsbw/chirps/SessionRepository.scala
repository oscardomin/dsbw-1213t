package dsbw.chirps

import org.bson.types.ObjectId
import dsbw.mongo.MongoDao
import java.util.Date

/**
 * Created with IntelliJ IDEA.
 * User: nebur1989
 * Date: 22/10/13
 * Time: 17:52
 * To change this template use File | Settings | File Templates.
 */

case class SessionRecord(userId:ObjectId, _id:ObjectId = new ObjectId(), date:Date = new Date())

class SessionDao(db:DB) extends MongoDao[SessionRecord](db.sessions) {
}

class SessionRepository(dao: SessionDao) {

  def findById(id: ObjectId) = dao.findOneByID(id)

  def deleteSession(id: ObjectId) = dao.deleteByID(id)

  def findByUserId(userId: ObjectId) = dao.findOneByParam("userId",userId)

  def saveSession(session:SessionRecord) = {
    dao.save(session)
  }

  def saveSession(id: ObjectId, userId: ObjectId) = {
    var s = new SessionRecord(userId,id)
    dao.save(s)
  }

}