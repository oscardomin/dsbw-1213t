package dsbw.chirps

import java.security.MessageDigest
import scala.collection.mutable
import org.specs2.time.Time
import javax.xml.datatype.Duration
import org.bson.types.ObjectId
import java.util.Date
import scala.collection.mutable.ListBuffer
import dsbw.json.JSON
import com.novus.salat.dao.SalatMongoCursor
import scala.tools.cmd.gen.AnyVals
import org.ocpsoft.prettytime.PrettyTime
import java.util.Locale

class Logger {
  def log(message:String) = {
    println("--- Message ---")
    println(message)
  }

  def logError(errorType:String,message:String) = {
    println("--- Error ---")
    println("Type: " + errorType)
    println("Message: " + message)
  }

  def logException(e:Exception) = {
    println("--- Exception ---")
    println("Message:" + e.getMessage)
    println("Stack Trace:")
    println(e.getStackTrace)
  }
}

/* Data Transfer Objects */

case class ReplyDTO(id:String, chirper:ChirperDTO, author:ChirperDTO, date:String, message:String, hashtags:Array[String], favorite:Boolean)
case class ChirpDTO(id:String, chirper:ChirperDTO, author:ChirperDTO, date:String, message:String, hashtags:Array[String], favorite:Boolean, replies:Array[ReplyDTO])
case class ChirperDTO(id: String, name:String, username:String, avatar:String, age: Int, status: String)
case class SessionDTO(id: String, userData: ChirperDTO)
case class MessageDTO(id:String, from:ChirperDTO, to:ChirperDTO, message:String, read:Int,sent:String)

class ChirpsService(chirpsRepository: ChirpsRepository, chirpersRepository: ChirpersRepository, sessionRepository: SessionRepository, log:Logger, messageRepository:MessageRepository) {

  var prettyDate = new PrettyTime(new Locale("en"));

  /* Private (util) functions */

  /* Error propagation */

  private def throwInternalServerError(e:Exception) = {
    log.logException(e)
    throw new Exception("500 Internal server error")
  }

  private def throwBadSession() = {
    throw new Exception("401 User not logged")
  }

  private def throwSessionExpired() = {
    throw new Exception("401 Session Expired")
  }

  private def throwInvalidParameter(message:String) = {
    throw new Exception("400 " + message)
  }

  /* Check Database */
  private def usernameExist(username:String):Boolean = {
    val chirperOption = {
      try {chirpersRepository.findByUsername(username)}
      catch {case e:Exception => throwInternalServerError(e)}
    }
    if(chirperOption.isEmpty)
      return false
    else
      return true
  }

  /* Common Access to Data Base */

  private def getChirperById(id:String):ChirperRecord = {
    return getChirperById(new ObjectId(id))
  }

  private def getChirperById(id:ObjectId):ChirperRecord = {
    val chirperOption = {
      try {chirpersRepository.findById(id)}
      catch {case e:Exception => throwInternalServerError(e)}
    }
    if(chirperOption.isEmpty){
      log.logError("Instance not exist","The Chirper with ID: " + id.toString + " not exist")
      throwInvalidParameter("Invalid Chirper")
    }
    return chirperOption.get
  }

  private def getChirperByUsername(username:String):ChirperRecord = {
    val chirperOption = {
      try {chirpersRepository.findByUsername(username)}
      catch {case e:Exception => throwInternalServerError(e)}
    }
    if(chirperOption.isEmpty){
      log.logError("Instance not exist","The Chirper with Username: " + username + " not exist")
      throwInvalidParameter("Invalid Chirper")
    }
    return chirperOption.get
  }

  private def getChirpById(id:String):ChirpRecord = {
    return getChirpById(new ObjectId(id))
  }

  private def getChirpById(id:ObjectId):ChirpRecord = {
    val chirpOption = {
      try {chirpsRepository.findById(id)}
      catch {case e:Exception => throwInternalServerError(e)}
    }
    if(chirpOption.isEmpty){
      log.logError("Instance not exist","The Chirp with ID: " + id.toString + " not exist")
      throwInvalidParameter("Invalid Chirp")
    }
    return chirpOption.get
  }

  private def getHashtagChirps(hashtag:String):List[ChirpRecord] = {
    val chirps = {
      try {chirpsRepository.findByHashtag(hashtag)}
      catch {case e:Exception => throwInternalServerError(e)}
    }
    return chirps
  }

  private def getUserChirps(userId:String):List[ChirpRecord] = {
    return getUserChirps(new ObjectId(userId))
  }

  private def getUserChirps(userId: ObjectId):List[ChirpRecord] = {
    val chirps = {
      try {chirpsRepository.findUserChirps(userId)}
      catch {case e:Exception => throwInternalServerError(e)}
    }
    return chirps
  }

  private def getUsersChirps(usersId: Seq[ObjectId]):List[ChirpRecord] = {
    val chirps = {
      try {chirpsRepository.findUsersChirps(usersId)}
      catch {case e:Exception => throwInternalServerError(e)}
    }
    return chirps
  }

  private def getSessionById(id:String):SessionRecord = {
    return getSessionById(new ObjectId(id))
  }

  private def getSessionById(id:ObjectId):SessionRecord = {
    val sessionOption = {
      try {sessionRepository.findById(id)}
      catch {case e:Exception => throwInternalServerError(e)}
    }
    if(sessionOption.isEmpty){
      log.logError("Instance not exist","The Session with ID: " + id.toString + " not exist")
      throwInvalidParameter("Invalid Session")
    }
    return sessionOption.get
  }

  /* Transformation Record to DTO */

  private def ChirperToDTO(record:ChirperRecord):ChirperDTO = {
    return ChirperDTO(record._id.toString,record.name,record.username,record.avatar,record.age,record.status)
  }

  private def ReplyToDTO(record:ChirpRecord):ReplyDTO = {
    val chirper = ChirperToDTO(getChirperById(record.chirper))
    val author = ChirperToDTO(getChirperById(record.author))
    val prettyTime = prettyDate.format(record.date)
    return ReplyDTO(record._id.toString,chirper,author,prettyTime,record.message,record.hashtags.toArray,false)
  }

  private def ChirpListToDTO(records:List[ChirpRecord]):List[ChirpDTO] = {
    var dtoList:List[ChirpDTO] = List()
    for(record <- records){
      val chirpDTO = ChirpToDTO(record)
      if(!dtoList.exists(dto => dto.id == chirpDTO.id)){
        dtoList = dtoList :+ chirpDTO
      }
    }
    return dtoList
  }

  private def ChirpToDTO(record:ChirpRecord):ChirpDTO = {
    if(record.replyTo != null)
      return ChirpToDTO(getChirpById(record.replyTo))
    val chirper = ChirperToDTO(getChirperById(record.chirper))
    val author = ChirperToDTO(getChirperById(record.author))
    val prettyTime = prettyDate.format(record.date)
    val replies = record.replies.map(reply => ReplyToDTO(getChirpById(reply))).toArray
    return ChirpDTO(record._id.toString,chirper,author,prettyTime,record.message,record.hashtags.toArray,false,replies)
  }

  private def SessionToDTO(record:SessionRecord):SessionDTO = {
    val user = ChirperToDTO(getChirperById(record.userId))
    return SessionDTO(record._id.toString,user)
  }


  private def MsgToDTO(record:MessageRecord):MessageDTO = {
    val to = ChirperToDTO(getChirperById(record.to))
    val from = ChirperToDTO(getChirperById(record.from))
    return MessageDTO(record._id.toString,from,to,record.message,record.read,prettyDate.format(record.sent))
  }

  /* Session commons */

  private val ExpirationSessionPolicy:Long = 2*3600*1000

  // @return true if session exist and is OK, otherwise return false
  private def checkSession(session:SessionRecord,autoThrow:Boolean = true):Boolean = {
    val elapsedTime = (new Date()).getTime - session.date.getTime
    if(elapsedTime > ExpirationSessionPolicy){
      sessionRepository.deleteSession(session._id)
      if(autoThrow)
        throwSessionExpired()
      return false
    }
    return true
  }

  private def getUserBySession(sessionId:String):ChirperRecord = {
    return getUserBySession(new ObjectId(sessionId))
  }

  private def getUserBySession(sessionId:ObjectId):ChirperRecord = {
    // Find the session
    val session = getSessionById(sessionId)
    // Check session Expiration (itself propagate the error)
    checkSession(session)
    // Find the user
    return getChirperById(session.userId)
  }

  private def processPassword(password:String):Seq[Byte] = {
    var md = MessageDigest.getInstance("SHA-256")
    md.update(password.getBytes())
    return md.digest().toSeq
  }

  /* Hashtag processing */

  private def getHashtags(message:String):Seq[String] = {
    //val hashtagRegex = "#(\\w)+[\\s|\\z|\\n]".r
    val hashtagRegex = "#[A-za-z0-9]+".r
    var hashtags:Seq[String] = Seq()
    for{hashtag <- hashtagRegex.findAllIn(message)} {
      hashtags = hashtags :+ hashtag.substring(1).replaceAll("\\s","")
    }
    return hashtags
  }

  /* Favorites processing */

  private def markFavorites(sessionId:String, chirps:List[ChirpDTO]):List[ChirpDTO] = {
    val user = getUserBySession(sessionId)
    return markFavorites(user.favorites,chirps)
  }

  private def markFavorite(favorites:Seq[ObjectId], reply:ReplyDTO):ReplyDTO = {
    val isFavorite = favorites.contains(new ObjectId(reply.id))
    return ReplyDTO(reply.id,reply.chirper,reply.author,reply.date,reply.message,reply.hashtags,isFavorite)
  }

  private def markFavorite(favorites:Seq[ObjectId], chirp:ChirpDTO):ChirpDTO = {
    val isFavorite = favorites.contains(new ObjectId(chirp.id))
    val replies = chirp.replies.map(reply => markFavorite(favorites,reply))
    return ChirpDTO(chirp.id,chirp.chirper,chirp.author,chirp.date,chirp.message,chirp.hashtags,isFavorite,replies)
  }

  private def markFavorites(favorites:Seq[ObjectId], chirps:List[ChirpDTO]):List[ChirpDTO] = {
    return chirps.map(chirp => markFavorite(favorites,chirp))
  }

  /* Public functions */

  def getChirper(username: String):ChirperDTO = {
    return ChirperToDTO(getChirperByUsername(username))
  }

  def rechirp(sessionId:String, chirpId:String):ChirpDTO = {
    val user = getUserBySession(sessionId)
    val chirp = getChirpById(chirpId)

    val rechirpObj = ChirpRecord(user._id,chirp.author,chirp.message,chirp.hashtags)
    chirpsRepository.saveChirp(rechirpObj)

    return ChirpToDTO(rechirpObj)
  }

  def getAllChirps(sessionId:Option[String]):List[ChirpDTO] = {
    val chirps = chirpsRepository.findAll
    val chirpsDTO = ChirpListToDTO(chirps)//chirps.map(chirp => ChirpToDTO(chirp))
    if(sessionId.isDefined)
      return markFavorites(sessionId.get,chirpsDTO)
    else
      return chirpsDTO
  }

  def listHashtagChirps(sessionId:Option[String], hashtag:String):List[ChirpDTO] = {
    val chirps = getHashtagChirps(hashtag)
    val chirpsDTO = ChirpListToDTO(chirps)//chirps.map(chirp => ChirpToDTO(chirp))
    if(sessionId.isDefined)
      return markFavorites(sessionId.get,chirpsDTO)
    else
      return chirpsDTO
  }

  def getPersonalTimeline(sessionId: String) :List[ChirpDTO] = {
    val user = getUserBySession(sessionId)
    val chirps = getUserChirps(user._id)
    val chirpsDTO = ChirpListToDTO(chirps)//chirps.map(chirp => ChirpToDTO(chirp))
    return markFavorites(user.favorites,chirpsDTO)
  }

  def getUserTimeline(sessionId:Option[String], username: String) :List[ChirpDTO] = {
    val user = getChirperByUsername(username)
    val chirps = getUserChirps(user._id)
    val chirpsDTO = ChirpListToDTO(chirps)//chirps.map(chirp => ChirpToDTO(chirp))
    if(sessionId.isDefined)
      return markFavorites(sessionId.get,chirpsDTO)
    else
      return chirpsDTO
  }

  def getTimeline(sessionId:String) :List[ChirpDTO] = {
    val user = getUserBySession(sessionId)
    val chirps = getUsersChirps(user.followings.+:(user._id))
    val chirpsDTO = ChirpListToDTO(chirps)//chirps.map(chirp => ChirpToDTO(chirp))
    return markFavorites(user.favorites,chirpsDTO)
  }

  def newChirp(sessionId: String, message: String):ChirpDTO = {
    val user = getUserBySession(sessionId)
    val hashtags = getHashtags(message)
    val chirp = ChirpRecord(user._id,user._id,message,hashtags)
    chirpsRepository.saveChirp(chirp)
    return ChirpToDTO(chirp)
  }

  def replyChirp(sessionId: String, replyTo: String, message: String):ChirpDTO = {
    val user = getUserBySession(sessionId)
    val hashtags = getHashtags(message)
    val chirp = getChirpById(replyTo)
    val reply = ChirpRecord(user._id,user._id,message,hashtags,chirp._id)
    chirpsRepository.saveChirp(reply)
    chirpsRepository.addReply(reply.replyTo,reply._id)
    return markFavorite(user.favorites,ChirpToDTO(chirpsRepository.findById(reply.replyTo).get))
  }

  def newChirper(username: String, name: String, avatar: String, age: Int, password: String):ChirperDTO = {
    // check if username exists
    if(usernameExist(username))
      throwInvalidParameter("Username already exist")

    // Generate the message digest
    val digestPassword = processPassword(password)
    // Common values
    val defaultMessage = "New in chirper!"

    // Generate ChirperRecord and save it
    val chirper = ChirperRecord(username,name,avatar,defaultMessage,age,digestPassword)
    chirpersRepository.saveChirper(chirper)

    return ChirperToDTO(chirper)
  }

  def newSession(username: String, password: String):SessionDTO = {
    // check if username exists
    if(!usernameExist(username))
      throwInvalidParameter("Username is invalid")
      //throwInvalidParameter("Username or password is invalid")
    // Get Chirper
    val user = getChirperByUsername(username)
    // Process Password
    val digestPassword = processPassword(password)
    if(digestPassword != user.password)
      throwInvalidParameter("password is invalid")
      //throwInvalidParameter("Username or password is invalid")
    // Delete Last Session
    val lastSessionOption = {
      try{sessionRepository.findByUserId(user._id)}
      catch {case e:Exception => throwInternalServerError(e)}
    }
    if(lastSessionOption.nonEmpty){
      sessionRepository.deleteSession(lastSessionOption.get._id)
    }
    // Create New Session
    val session = new SessionRecord(user._id)
    sessionRepository.saveSession(session)

    return SessionToDTO(session)
  }

  def closeSession(sessionId:String) = {
    val session = getSessionById(sessionId)
    if(checkSession(session,false)){
      sessionRepository.deleteSession(session._id)
    }
  }

  def follow(sessionId: String, followingUsername: String) = {
    val user = getUserBySession(sessionId)
    val followUser = getChirperByUsername(followingUsername)
    if(!user.followings.contains(followUser._id)){
      try{
        chirpersRepository.addFollow(user._id,followUser._id)
        chirpersRepository.addFollower(followUser._id,user._id)
      }
      catch {case e:Exception => throwInternalServerError(e)}
    }
  }

  def unfollow(sessionId: String, followingUsername: String) = {
    val user = getUserBySession(sessionId)
    val followUser = getChirperByUsername(followingUsername)
    if(user.followings.contains(followUser._id)){
      try{
        chirpersRepository.removeFollow(user._id,followUser._id)
        chirpersRepository.removeFollower(followUser._id,user._id)
      }
      catch {case e:Exception => throwInternalServerError(e)}
    }
  }

  def getFollowees(sessionId:String):List[ChirperDTO] = {
    val user = getUserBySession(sessionId)
    val followees = chirpersRepository.getFollowings(user._id)
    return followees.map(id => ChirperToDTO(getChirperById(id))).toList
  }

  def getFavorites(sessionId:String):List[ChirpDTO] = {
    val user = getUserBySession(sessionId)

    val favorites = chirpersRepository.getFavorites(user._id)
    val chirps = favorites.map(id => getChirpById(id)).toList.sortWith(_.date after _.date)
    val chirpsDTO = chirps.map(chirp => ChirpToDTO(chirp)).toList
    return markFavorites(sessionId,chirpsDTO)
  }

  def addFavorite(sessionId:String, chirp:String) = {
    val user = getUserBySession(sessionId)
    val favorite = getChirpById(chirp)
    if(!user.favorites.contains(favorite._id)){
      try{chirpersRepository.addFavorite(user._id,favorite._id)}
      catch {case e:Exception => throwInternalServerError(e)}
    }
  }

  def deleteFavorite(sessionId:String, chirp:String) = {
    val user = getUserBySession(sessionId)
    val favorite = getChirpById(chirp)
    if(user.favorites.contains(favorite._id)){
      try{chirpersRepository.removeFavorite(user._id,favorite._id)}
      catch {case e:Exception => throwInternalServerError(e)}
    }
  }

  def sendMessage(sessionId:String, to:String,msg:String) = {
    val user = getUserBySession(sessionId)
    val receiver = getChirperByUsername(to)
    try{messageRepository.send(user._id,receiver._id, msg)}
    catch {case e:Exception => throwInternalServerError(e)}
  }

  def readInbox(sessionId:String):List[MessageDTO] = {
    val user = getUserBySession(sessionId)
    val msgs = messageRepository.read(user._id)
    return msgs.map(msg => MsgToDTO(msg)).toList
  }

  def numPM(sessionId:String):Int = {
    val user = getUserBySession(sessionId)
    return messageRepository.numPM(user._id)
  }

}
