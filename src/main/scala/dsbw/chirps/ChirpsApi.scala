package dsbw.chirps

import dsbw.server.{Server, HttpStatusCode, Response, Api}
import dsbw.json.JSON
import Config.{dbHostName, dbPort, dbName, username, pwd, webServerPort}
import org.bson.types.ObjectId
import java.util.Date
import scala.util.matching.Regex

/* Request Objects */

case class ChirpRequest(message:String)
case class ChirpRequestSession(sessionId:String,message:String)

case class ChirperRequest(username: String, name: String, age: String, avatar: String, password: String)
case class SessionRequest(username: String, password: String)

case class SessionCloseRequest(sessionId: String)

case class FollowRequest(follower: String)
case class FollowRequestSession(sessionId: String, follower: String)

case class ChirperTimelinePublicRequest(username: String)

case class ChirperTimelinePrivateRequest(sessionId: String)

case class ReChirpRequest(chirpId:String)
case class ReChirpRequestSession(sessionId:String, chirpId:String)
case class HashtagRequest(hashtag:String)

case class FolloweesRequest(sessionId:String)

case class FavoritesRequest(sessionId:String)
case class FavoriteRequest(favorite:String)
case class FavoriteRequestSession(sessionId:String, favorite:String)

case class MessageRequestSession(sessionId:String, message:String, to:String)
case class MessagesRequest(sessionId:String)

case class ReplyChirpSession(sessionId:String, replyTo:String, message:String)
case class ReplyChirp(replyTo:String, message:String)

/* Header content */
case class Header(credential:Option[String], page:Option[String], elements:Option[String])

/** Chirps API */
class ChirpsApi(chirpsService:ChirpsService, log:Logger) extends Api {

  private val hashtagRegex = "/api/hashtag/.*".r

  /* Function to check if the string matches with the regex pattern */
  def matches(str:String, reg:Regex):Boolean = {
    str match{
      case reg() => true
      case _ => false
    }
  }

  private val MapGetFunctions:Map[String, (Header) => Response] = Map(
    "/api/chirps" -> getChirps
  )

  /*
   * To add Get methods by regex use this convention:
   * Regex for matching the uri -> (Function that resolves the query, Function that give the parameter)
   */
  private val MapGetFunctionsRegex:Map[Regex, ((String,Header) => Response,(String) => String)] = Map(
    "/api/user/.*".r -> (getChirper,(uri:String) => uri.substring(10))
  )

  /*
   * To add Post methods use this convention:
   * String of the method path -> Function that resolves the query
   */
  private val MapPOSTFunctions:Map[String, (Option[JSON],Header) => Response] = Map(
    "/api/newChirp" -> createChirp,
    "/api/newUser" -> createChirper,
    "/api/login" -> login,
    "/api/logout" -> logout,
    "/api/follow" -> follow,
    "/api/unfollow" -> unfollow,
    "/api/myTimeline" -> getMyTimeLine,
    "/api/ownTimeline" -> getOwnTimeLine,
    "/api/userTimeline" -> getUserTimeLine,
    "/api/rechirp" -> rechirp,
    "/api/hashtag" -> getHashtagChirps,
    "/api/getFollowees" -> getFollowees,
    "/api/getFavorites" -> getFavorites,
    "/api/favorite" -> favorite,
    "/api/unfavorite" -> unfavorite,
    "/api/send" -> send,
    "/api/replyChirp" -> replyChirp,
    "/api/privateMessages" -> read,
    "/api/numPM" -> numPM
    //"/api/read" -> read
  )

  def service(method: String, uri: String, parameters: Map[String, List[String]] = Map(), headers: Map[String, String] = Map(), body: Option[JSON] = None): Response = {

    val header = new Header(headers.get("Session"),
                            headers.get("Page"),
                            headers.get("Elements"))

    if(method == "GET"){
      val function = MapGetFunctions.get(uri)
      if(function.isEmpty){
        val pair = MapGetFunctionsRegex.collectFirst{case (pattern,v) if(matches(uri,pattern)) => v}
        if(pair == None){
          log.logError("Message not match","GET Method (" + uri + ") not exist")
          return Response(HttpStatusCode.BadRequest,"GET Method not exist")
        }
        val function = pair.get._1
        val process = pair.get._2
        return callGETServicePattern(process(uri),header,function)
      }
      callGetService(header,function.get)
    }else if(method == "POST"){
      val function = MapPOSTFunctions.get(uri)
      if(function.isEmpty){
        log.logError("Message not match","POST Method (" + uri + ") not exist")
        return Response(HttpStatusCode.BadRequest,"POST Method not exist")
      }
      return callPOSTService(body,header,function.get)
    }else{
      log.logError("Unsuported method",method + " is not suported")
      Response(HttpStatusCode.BadRequest,"Unsuported method")
    }
  }

  private def generateErrorResponse(e:Exception):Response = {
    val message = e.getMessage
    val status = message.substring(0,3)
    val error = message.substring(4)
    status match {
      case "400" => {
        log.logError("Bad request",error)
        return Response(HttpStatusCode.BadRequest,error)
      }
      case "401" => {
        log.logError("Unauthorized access",error)
        Response(HttpStatusCode.Unauthorized,error)
      }
      case "500" => {
        log.logError("Internal Server Error",error)
        Response(HttpStatusCode.InternalServerError,error)
      }
      case _ => {
        log.logException(e)
        Response(HttpStatusCode.InternalServerError,"Internal Server Error")
      }
    }
  }

  private def callGetService(header:Header, function:(Header) => Response):Response = {
    try{
      function(header)
    }catch{
      case e:Exception =>
        return generateErrorResponse(e)
    }
  }

  private def callGETServicePattern(content:String, header:Header, function:(String,Header) => Response):Response = {
    try{
      function(content,header)
    }catch{
      case e:Exception =>
        return generateErrorResponse(e)
    }
  }

  private def callPOSTService(body: Option[JSON], header:Header, function:(Option[JSON],Header) => Response):Response = {
    if (body == None){
      log.logError("Body Empty","POST request without body")
      return Response(HttpStatusCode.BadRequest,"POST request without body")
    }
    try{
      return function(body,header)
    }catch{
      case e:Exception =>
        return generateErrorResponse(e)
    }
  }

  /*
   * Function definitions
   */

  private def getHashtagChirps(body: Option[JSON], header:Header): Response = {
    val hashtag = JSON.fromJSON[HashtagRequest](body.get).hashtag.substring(1)
    val responseBody = chirpsService.listHashtagChirps(header.credential,hashtag)
    return Response(HttpStatusCode.Ok, responseBody)
  }

  private def rechirp(body: Option[JSON], header:Header) : Response = {
    val sessionId = {
      if(header.credential.isDefined) header.credential.get
      else JSON.fromJSON[ReChirpRequestSession] (body.get).sessionId
    }
    val id = {
      if(header.credential.isDefined) JSON.fromJSON[ReChirpRequest](body.get).chirpId
      else JSON.fromJSON[ReChirpRequestSession](body.get).chirpId
    }
    return Response(HttpStatusCode.Ok, chirpsService.rechirp(sessionId, id))
  }

  private def getChirps(header:Header): Response = {
    val responseBody = chirpsService.getAllChirps(header.credential)
    return Response(HttpStatusCode.Ok, responseBody)
  }

  private def follow(body: Option[JSON], header:Header): Response = {
    val sessionId = {
      if(header.credential.isDefined) header.credential.get
      else JSON.fromJSON[FollowRequestSession](body.get).sessionId
    }
    val follower = {
      if(header.credential.isDefined) JSON.fromJSON[FollowRequest](body.get).follower
      else JSON.fromJSON[FollowRequestSession](body.get).follower
    }
    chirpsService.follow(sessionId,follower)
    return Response(HttpStatusCode.Ok)
  }

  private def unfollow(body: Option[JSON], header:Header): Response = {
    val sessionId = {
      if(header.credential.isDefined) header.credential.get
      else JSON.fromJSON[FollowRequestSession](body.get).sessionId
    }
    val follower = {
      if(header.credential.isDefined) JSON.fromJSON[FollowRequest](body.get).follower
      else JSON.fromJSON[FollowRequestSession](body.get).follower
    }
    chirpsService.unfollow(sessionId,follower)
    return Response(HttpStatusCode.Ok)
  }

  private def login(body: Option[JSON], header:Header): Response = {
    val username = JSON.fromJSON[SessionRequest](body.get).username
    val password = JSON.fromJSON[SessionRequest](body.get).password
    val responseBody = chirpsService.newSession(username,password)
    return Response(HttpStatusCode.Ok,responseBody)
  }

  private def logout(body: Option[JSON], header:Header): Response = {
    val sessionId = {
      if(header.credential.isDefined) header.credential.get
      else JSON.fromJSON[SessionCloseRequest](body.get).sessionId
    }
    chirpsService.closeSession(sessionId)
    return Response(HttpStatusCode.Ok)
  }

  private def getChirper(username: String, header: Header):Response = {
    val responseBody = chirpsService.getChirper(username)
    return Response(HttpStatusCode.Ok,responseBody)
  }

  private def createChirper(body: Option[JSON], header:Header):Response = {
    val username = JSON.fromJSON[ChirperRequest](body.get).username
    val name = JSON.fromJSON[ChirperRequest](body.get).name
    val age = JSON.fromJSON[ChirperRequest](body.get).age.toInt
    val avatar = JSON.fromJSON[ChirperRequest](body.get).avatar
    val password = JSON.fromJSON[ChirperRequest](body.get).password
    val responseBody = chirpsService.newChirper(username,name,avatar,age,password)
    return Response(HttpStatusCode.Ok,responseBody)
  }

  private def createChirp(body: Option[JSON], header:Header): Response = {
    val sessionId = {
      if(header.credential.isDefined) header.credential.get
      else JSON.fromJSON[ChirpRequestSession](body.get).sessionId
    }
    val message = {
      if(header.credential.isDefined) JSON.fromJSON[ChirpRequest](body.get).message
      else JSON.fromJSON[ChirpRequestSession](body.get).message
    }
    val responseBody = chirpsService.newChirp(sessionId, message)
    return Response(HttpStatusCode.Ok, responseBody)
  }

  private def getMyTimeLine(body: Option[JSON], header:Header): Response = {
    val sessionId = {
      if(header.credential.isDefined) header.credential.get
      else JSON.fromJSON[ChirperTimelinePrivateRequest](body.get).sessionId
    }
    val responseBody = chirpsService.getTimeline(sessionId)
    return Response(HttpStatusCode.Ok,responseBody)
  }

  private def getOwnTimeLine(body: Option[JSON], header:Header): Response = {
    val sessionId = {
      if(header.credential.isDefined) header.credential.get
      else JSON.fromJSON[ChirperTimelinePrivateRequest](body.get).sessionId
    }
    val responseBody = chirpsService.getPersonalTimeline(sessionId)
    return Response(HttpStatusCode.Ok,responseBody)
  }

  private def getUserTimeLine(body: Option[JSON], header:Header): Response = {
    val username = JSON.fromJSON[ChirperTimelinePublicRequest](body.get).username
    val responseBody = chirpsService.getUserTimeline(header.credential,username)
    return Response(HttpStatusCode.Ok,responseBody)
  }

  private def getFollowees(body: Option[JSON], header:Header): Response = {
    val sessionId = {
      if(header.credential.isDefined) header.credential.get
      else JSON.fromJSON[FolloweesRequest](body.get).sessionId
    }
    val responseBody = chirpsService.getFollowees(sessionId)
    return Response(HttpStatusCode.Ok,responseBody)
  }

  private def getFavorites(body: Option[JSON], header:Header): Response = {
    val sessionId = {
      JSON.fromJSON[FavoritesRequest](body.get).sessionId
    }
    val responseBody = chirpsService.getFavorites(sessionId)
    return Response(HttpStatusCode.Ok,responseBody)
  }

  private def favorite(body: Option[JSON], header:Header): Response = {
    val sessionId = {
      if(header.credential.isDefined) header.credential.get
      else JSON.fromJSON[FavoriteRequestSession](body.get).sessionId
    }
    val favorite = {
      if(header.credential.isDefined) JSON.fromJSON[FavoriteRequest](body.get).favorite
      else JSON.fromJSON[FavoriteRequestSession](body.get).favorite
    }
    chirpsService.addFavorite(sessionId,favorite)
    return Response(HttpStatusCode.Ok)
  }

  private def unfavorite(body: Option[JSON], header:Header): Response = {
    val sessionId = {
      if(header.credential.isDefined) header.credential.get
      else JSON.fromJSON[FavoriteRequestSession](body.get).sessionId
    }
    val favorite = {
      if(header.credential.isDefined) JSON.fromJSON[FavoriteRequest](body.get).favorite
      else JSON.fromJSON[FavoriteRequestSession](body.get).favorite
    }
    chirpsService.deleteFavorite(sessionId,favorite)
    return Response(HttpStatusCode.Ok)
  }

  private def send(body: Option[JSON], header:Header): Response = {
    println("API send")
    val sessionId = {
      JSON.fromJSON[MessageRequestSession](body.get).sessionId
    }
    val to = {
      JSON.fromJSON[MessageRequestSession](body.get).to
    }
    val msg = {
      JSON.fromJSON[MessageRequestSession](body.get).message
    }
    chirpsService.sendMessage(sessionId,to,msg)
    return Response(HttpStatusCode.Ok)
  }

  private def read(body: Option[JSON], header:Header): Response = {
    val sessionId = {
      JSON.fromJSON[MessagesRequest](body.get).sessionId
    }
    val responseBody = chirpsService.readInbox(sessionId)
    return Response(HttpStatusCode.Ok,responseBody)
  }

  private def numPM(body: Option[JSON], header:Header): Response = {
    val sessionId = {
       JSON.fromJSON[MessagesRequest](body.get).sessionId
    }
    val responseBody = chirpsService.numPM(sessionId)
    return Response(HttpStatusCode.Ok,responseBody)
  }

  private def replyChirp(body: Option[JSON], header:Header): Response = {
    val sessionId = {
      if(header.credential.isDefined) header.credential.get
      else JSON.fromJSON[ReplyChirpSession](body.get).sessionId
    }
    val replyTo = {
      if(header.credential.isDefined) JSON.fromJSON[ReplyChirp](body.get).replyTo
      else JSON.fromJSON[ReplyChirpSession](body.get).replyTo
    }
    val message = {
      if(header.credential.isDefined) JSON.fromJSON[ReplyChirp](body.get).message
      else JSON.fromJSON[ReplyChirpSession](body.get).message
    }
    val responseBody = chirpsService.replyChirp(sessionId,replyTo,message)
    return Response(HttpStatusCode.Ok,responseBody)
  }
}

object ChirpsApp extends App {

  val log = new Logger()
  val db = new DB(dbHostName, dbPort, dbName, username, pwd)
  val chirpsRepository = new ChirpsRepository(new ChirpsDao(db))
  val chirpersRepository = new ChirpersRepository(new ChirpersDao(db))
  val sessionRepository = new SessionRepository(new SessionDao(db))
  val messageRepository = new MessageRepository(new MessageDao(db))
  val chirpsService = new ChirpsService(chirpsRepository,chirpersRepository,sessionRepository,log,messageRepository)

  val server = new Server(new ChirpsApi(chirpsService,log), webServerPort)
  server.start()

}
