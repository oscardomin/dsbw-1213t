package dsbw.mongo

import com.mongodb.casbah.MongoCollection
import com.novus.salat.dao.{SalatMongoCursor, SalatDAO}
import org.bson.types.ObjectId
import com.mongodb.casbah.Imports._
import com.mongodb.MongoException.DuplicateKey
import com.novus.salat.Context
import com.mongodb.casbah.commons.MongoDBObject
import dsbw.mongo.SalatContext.ctx


/** Generic DAO class used as a superclass of MongoDB DAOs  */
class MongoDao[ObjectType <: AnyRef](collection: MongoCollection)(implicit mot: Manifest[ObjectType]) {

  val salatDao = new SalatDAO[ObjectType,ObjectId](collection){}
  val sort = MongoDBObject("date" -> -1)

  /** Check the last operation performend and, in case of error, throw an exception */
  def checkLastError() {
    val lastError = collection.getLastError()
    try{
      lastError.throwOnError()
    }catch{
      case m:DuplicateKey => {
        val errorMsg = lastError.getString("err")
        val index = collection.indexInfo.find(ii => (ii.get("unique") == true) && errorMsg.contains("%s.$%s".format(ii.get("ns"),ii.get("name"))))
        index.map(ii => throw new DuplicateKeyException(collection.name,ii.get("name").asInstanceOf[String]))
        throw m
      }
      case t:Throwable => throw t
    }
  }

  /** Delete a document by their ID */
  def deleteByID(id:ObjectId) = salatDao.removeById(id)

  /** Find a document using a query and return it or None if it wasn't found */
  def findOne[T<: AnyRef](query:Map[String,T]): Option[ObjectType] = salatDao.findOne(query)

  /** Find a document by id and return it or None if it wasn't found */
  def findOneByID(id:ObjectId): Option[ObjectType] = salatDao.findOneById(id)

  /** Find a collection of documents given a query, or None if none of them complied the query*/
  def find[T<: AnyRef](query:Map[String, T]) : SalatMongoCursor[ObjectType] = salatDao.find(query).sort(orderBy = sort)

  /** Find a collection of documents given a query composed of a set of fields (used for the timeline)*/
  def findSubQuery[T<: AnyRef](what:String,query:Set[ObjectId]) : SalatMongoCursor[ObjectType] = salatDao.find(what $in query).sort(orderBy = sort)

  /** Find a collection of documents given their ids */
  def findByIds(ids:Set[ObjectId]) =  salatDao.find("_id" $in ids).sort(orderBy = sort).toSet

  /** Find a document given parameter and they value */
  def findOneByParam(param: String, value: Object):Option[ObjectType] = findOne(Map((param,value)))

  /** Find a collection of documents given parameter and they value */
  def findByParam(param: String, value: Object):SalatMongoCursor[ObjectType] = salatDao.find(Map((param,value))).sort(orderBy = sort)

  /** Find all the documents in a collection */
  def findAll = salatDao.find((MongoDBObject.empty)).sort(orderBy = sort)

  def updateParamByID(id:ObjectId, param:String, value:Object) {
    updateById(id,MongoDBObject("$set" -> MongoDBObject(param -> value)))
  }
  def updateParamByID2(id:ObjectId, param:String, value:Int) {
    updateById(id,MongoDBObject("$set" -> MongoDBObject(param -> value)))
  }

  /** Update the documents matching a query with the given update */
  def update[T<: AnyRef](query:Map[String,T],update:MongoDBObject,multi:Boolean=true) {
    salatDao.update(query,update,multi=multi)
  }

  /** Update a document identified by its id with the given update */
  def updateById(id:ObjectId,update:MongoDBObject){
    this.update(Map("_id"->id),update,multi=false)
  }

  /** Updatea collection of documents identified by their ids with the given update */
  def updateByIds(ids:Set[ObjectId],update:MongoDBObject){
    salatDao.update("_id" $in ids,update,multi=true)
  }

  /** Save a document */
  def save(obj:ObjectType) {
    salatDao.save(obj)
  }

}

case class DuplicateKeyException(collection:String, key:String) extends RuntimeException{

  override def toString = """DuplicateKeyException(collection="%s",key="%s")""".format(collection,key)
}
